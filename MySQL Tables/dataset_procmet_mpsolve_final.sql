-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: dataset_procmet
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mpsolve_final`
--

DROP TABLE IF EXISTS `mpsolve_final`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mpsolve_final` (
  `release_number` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `fcomm` varchar(255) DEFAULT NULL,
  `fadev` varchar(255) DEFAULT NULL,
  `fddev` varchar(255) DEFAULT NULL,
  `fexp` varchar(255) DEFAULT NULL,
  `foexp` varchar(255) DEFAULT NULL,
  `fmodd` varchar(255) DEFAULT NULL,
  `faddl` varchar(255) DEFAULT NULL,
  `freml` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mpsolve_final`
--

LOCK TABLES `mpsolve_final` WRITE;
/*!40000 ALTER TABLE `mpsolve_final` DISABLE KEYS */;
INSERT INTO `mpsolve_final` VALUES ('3.0.1',' __cplusplus','1','1','1','2','2','1','2','0','clean'),('3.1.2',' __cplusplus','3','1','2','100','104','3','34','0','clean'),('3.1.4',' __cplusplus','4','1','3','18','2000','11','91','90','clean'),('3.0.1','_mps_private ','1','1','1','605','66','1','55','11','clean'),('3.1.4','_mps_private ','7','1','2','3','3566','17','104','105','clean'),('3.0.1','have_graphical_debugger','1','1','1','1','40','2','13','6','clean'),('3.0.1','have_gtk','8','1','1','213','412','10','36','5','defective'),('3.0.1','not __windows','1','1','1','924','80','1','66','14','clean'),('3.1.2','not __windows','6','1','2','4','3129','8','132','259','defective'),('3.1.4','not __windows','5','1','3','1','729','6','66','55','defective'),('3.0.1','not disable_debug','1','1','1','24','24','1','24','0','clean'),('3.1.4','not disable_debug','14','1','2','7','1715','19','42','48','defective'),('3.0.1','not gtk_widget_get_allocated_height','2','1','1','34','102','2','50','0','clean'),('3.0.1','not gtk_widget_get_allocated_width','3','1','1','34','106','3','35','0','clean'),('3.1.2','__cplusplus','3','1','1','29','37','5','6','0','clean'),('3.1.4','__cplusplus','15','1','2','34','5454','46','62','55','defective'),('3.1.5','__cplusplus','1','1','3','5','6','1','5','1','clean'),('3.1.2','__mps_debug','1','1','1','42','23','1','2','21','clean'),('3.1.2','__windows','1','1','1','13','13','1','13','0','clean'),('3.1.2','_mps_private','2','1','1','2','102','2','51','0','clean'),('3.1.4','_mps_private','12','1','2','1','5780','28','92','114','clean'),('3.1.2','have_sysconf','1','1','1','5','6','1','5','1','clean'),('3.1.2','have_vsnprintf','1','1','1','12','12','1','12','0','clean'),('3.1.4','have_vsnprintf','1','1','2','7410','173','1','95','78','clean'),('3.1.2','nice_debug','1','1','1','42','23','1','2','21','clean'),('3.1.4','nice_debug','5','1','2','36','448','5','45','44','clean'),('3.1.5','nice_debug','2','1','3','10','20','2','5','5','clean'),('3.1.2','not __cplusplus','1','1','1','15','15','1','15','0','clean'),('3.1.4','not __cplusplus','2','1','2','151','201','2','12','88','clean'),('3.1.2','not isinf','1','1','1','29','29','1','29','0','clean'),('3.1.4','not isinf','2','1','2','358776','2447','3','371','444','clean'),('3.1.2','not isnan','1','1','1','29','29','1','29','0','clean'),('3.1.4','not isnan','2','1','2','358776','2447','3','371','444','clean'),('3.1.2','not log2','1','1','1','4','15','3','4','1','clean'),('3.1.4','not log2','1','1','2','990','935','3','156','155','clean'),('3.1.2','not vsnprintf','1','1','1','12','12','1','12','0','clean'),('3.1.4','not vsnprintf','1','1','2','7410','173','1','95','78','clean'),('3.1.4',' __cplusplus  ','1','1','1','43681','418','1','209','209','clean'),('3.1.4','__cplusplus ','1','1','1','7055','168','1','83','85','clean'),('3.1.4','disable_debug','1','1','1','43681','418','1','209','209','clean'),('3.1.4','getline','1','1','1','135','135','1','135','0','clean'),('3.1.5','getline','1','1','2','5','5','1','5','0','clean'),('3.1.4','have_hidden_visibility_attribute','1','1','1','228','31','1','19','12','clean'),('3.1.4','mps_catch_fpe','1','1','1','99','20','1','11','9','clean'),('3.1.4','mps_parser_debug','4','1','1','154','361','5','54','18','defective'),('3.1.5','mps_parser_debug','1','1','2','476','48','1','34','14','clean'),('3.1.4','mps_publish_private_methods','2','1','1','228','1229','2','311','303','clean'),('3.1.4','mps_use_builtin_complex','1','1','1','7047','168','1','87','81','clean'),('3.1.4','mps_use_qml','6','1','1','11','47','6','5','2','clean'),('3.1.4','not __mps_cluster','1','1','1','24','14','1','12','2','clean'),('3.1.4','not __mps_mt_types','1','1','1','24','14','1','12','2','clean'),('3.1.4','not __mps_not_define_bool','2','1','1','358776','2447','3','371','444','clean'),('3.1.4','not _mps_private','3','1','1','42','212','4','53','0','clean'),('3.1.4','not disable_debug  ','1','1','1','1984','94','1','62','32','clean'),('3.1.4','not getline','1','1','1','10','10','1','10','0','clean'),('3.1.4','not have_fmemopen','7','1','1','214','1362','9','78','73','defective'),('3.1.4','not have_getline','1','1','1','135','256','2','69','58','defective'),('3.1.5','not have_getline','1','1','2','5','5','1','5','0','clean'),('3.1.4','not have_strndup','2','1','1','41','45','2','22','0','clean'),('3.1.4','not mpf_pow_ui','1','1','1','3364','116','1','58','58','clean'),('3.1.4','not mpf_swap','1','1','1','3364','116','1','58','58','clean'),('3.1.4','not mpq_out_str','1','1','1','3364','116','1','58','58','clean'),('3.1.4','not mpq_swap','1','1','1','3364','116','1','58','58','clean'),('3.1.4','not mps_mt_types_','2','1','1','24','16','2','6','1','clean'),('3.1.4','not mps_use_builtin_complex','1','1','1','3','4','1','3','1','clean'),('3.1.4','not mpz_swap','1','1','1','3364','116','1','58','58','clean'),('3.1.4','not mpz_tstbit','1','1','1','3364','116','1','58','58','clean'),('3.1.4','not rand_source','1','1','1','1','2','1','1','1','clean'),('3.1.4','not size_max','2','1','1','600','305','3','54','47','defective'),('3.1.4','not ssize_max','2','1','1','600','305','3','54','47','defective'),('3.1.4','rand_val','2','1','1','36','20','2','5','5','clean'),('3.1.5','not _isatty','2','1','1','10','20','2','5','5','clean'),('3.1.5','not isatty','2','1','1','10','20','2','5','5','clean');
/*!40000 ALTER TABLE `mpsolve_final` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-29 16:53:05
