# Online appendix for manuscript "Feature-Oriented Defect Prediction"

This is the online appendix for the manuscript of "Feature-Oriented Defect Prediction" submitted for the 24th SPLC in 2020. The paper was accepted in July 2020 and created by:

* Stefan Strüder | University of Koblenz-Landau, Germany | Responsible for the content of this repository
* Mukelabai Mukelabai | Chalmers University of Technology, Sweden
* Daniel Strüber | Radboud University, Netherlands
* Thorsten Berger | Chalmers University of Technology, Sweden


## Content 
* [Repository Structure](#repository-structure)
* [Introduction and Requirements](#introduction-and-requirements)
* [Data Retrieval and Processing](#data-retrieval-and-processing)
* [Calculation of Metrics and Finalization of Datasets](#calculation-of-metrics-and-finalization-of-datasets)
* [Templates](#templates)
* [Application of the Datasets](#application-of-the-datasets)
* [Special notice](#special-notice)

## Repository Structure

The repository is structured as follows:

* **Datasets** as used for training and testing (from the perspective of the ML framework, we have three different datasets, distinguished by the metric/attribute set being used)
* **MySQL Tables** created during dataset and metric creation
* **Results** - for each metric set and classifier, we provide the raw results for RQ1 and RQ3 as "classifier.txt", and the results for RQ2 as "classifier_ranking.txt"
* **Scripts** used for data processing and metrics computation

## Introduction and Requirements

The scripts in this repository can be used to reproduce the steps of creating the raw data, then calculating the metrics and finalizing the datasets. The created data sets can then be used to train and test classifiers. The raw data in the form of MySQL tables and the datasets are part of this repository. It also contains the results of applying the test datasets to the created classifiers.

To reproduce the steps correctly, several tools are required, which are listed below. For the preparation of the manuscript all steps were carried out in a Windows 10 system.

* Python 3.x | [Download](https://www.python.org/downloads/)
* PyDriller 1.15.2  by Davide Spadini et al. | Python library to extract data from Git repositories | [Download](https://github.com/ishepard/pydriller)
* MySQL Community 8.x, full installation | [Download](https://dev.mysql.com/downloads/) 
* MySQL Connector for Python 8.x | Install via pip: `pip install mysql-connector-python`
* WEKA Workbench 3.8 | [Download](https://waikato.github.io/weka-wiki/downloading_weka/)
* Git 2.28.0 | [Download](https://git-scm.com/download/win)
* Java JRE Version 8 | [Download](https://www.java.com/de/download/)

Please make sure that all tools (MySQL including a running server) and all required Python libraries are correctly installed before running the scripts.

## Data Retrieval and Processing

The data retrieval and processing is carried out with the script `1-processing_clean.py` and contains various steps:

1. Creation of database
2. Data retrieval with PyDriller followed by processing steps
3. Application of the SZZ-algorithm
4. Transfer of the results to the main table

To run the script via Command Line / PowerShell / Terminal use: `python 1-processing_clean.py`. Depending on the amount of data to be retrieved, the execution of the script may take some time.

**Please note:** Before the script can be executed, configurations must be made in it. These are located in lines 17 to 35, marked with `ENTER_HERE`, divided into three categories and read as follows:

*MySQL-database information*
 
  * `host` - server name or IP adress on which your MySQL-server is running (i.e. *localhost*)
  * `user` - username of MySQL-server (i.e. *root*)
  * `password` - password of MySQL-server
  * `database` - desired name of the database on which all data is stored

*Data retrieval*

* `software` - name of the software project for which data is being retrieved
* `path` - path to project's repository - local (path to folder, recommended) or external (URL to repository)
* `releases` - string-list of release numbers for which data is to be retrieved
* `from_tags` - lower limit tag list (strings) of the releases (in the same order as the releases list)
* `to_tags`- upper limit tag list (strings) of the releases (in the same order as the releases list)

*Data processing*

* `keep_szz_table` - decision (True / False) whether table with SZZ information should be kept

The resulting table is ready to be used for metrics calculation and dataset finalization. **The script needs to be executed for every software project.**

## Calculation of Metrics and Finalization of Datasets

The calculation of metrics and finalization of the datasets is split into three steps with one script each. The calculation of the structural metrics differs slightly.

### Calculation of Metrics

The calculation of the metrics is carried out with the script `2-metrics_clean.py`. The following metrics will be calculated:

* FCOMM
* FADEV
* FDDEV
* FEXP
* FOEXP
* FMODD
* FNLOC
* FCYCO
* FADDL
* FREML

To run the script via Command Line / PowerShell / Terminal use: `python 2-metrics_clean.py`. Depending on the amount of data, the execution of the script may take some time.

**Please note:** Before the script can be executed, configurations must be made in it. These are located in lines 10 to 23, marked with `ENTER_HERE` and read as follows:

*MySQL-database information*
 
  * `host` - server name or IP adress on which your MySQL-server is running (i.e. *localhost*)
  * `user` - username of MySQL-server (i.e. *root*)
  * `password` - password of MySQL-server
  * `database` - name of the database, which was previously assigned
  * `softwares` - list of names of software projects whose metrics should be calculated

The resulting metrics-tables are ready to be used for dataset finalization. In addition, a .csv file is output for each software project, which is intended as input for the following step. These files have the name `PROJECTNAME.csv` and can be found in the folder `structure_metrics_files`.

### Calculation of Structural Metrics

To derive the structural metrics (scattering degree (SCAT), tangling degree (TANGA), nesting depth (NDEP), and number of features in file (LOFC)), perform the following steps:

*Requirements*

* The tool FeatRacer together with its configuration properties file found in the folder `FeatRacer` (Note: the tool was built with Java 8, if you run into strange errors, please consider changing your version of Java)
* CSV files containing the last commit of each release. The metrics are calculated on the state of files at the last commit of each release of each subject project.
* Ensure that Git is installed and that `bash` is included in your class path. E.g., if bash.exe resides in C:/Program Files/Git/bin, add this path to the class variables on Windows by going to Computer->Advanced System Properties->Environmental Variables-->System Variables and editing the Path variable tp add a new entry for C:\Program Files\Git\bin. (linux systems don't need this step)

*Steps*

 1. Clone the subject repositories to the direcory of your choice using their project links provided in the paper.
 2. Download `FeatRacer` including its properties file
 3. Download the CSV files containing the commits (found in Scripts/structure_metrics_files)
 4. Go to the directory where you downloaded `FeatRacer` and open config.properties file
 5. Change line 2 by specifying the repository folders to be analysed. Here you must refer to directory paths of the projects you cloned in step 1 and separate them by commas. For instance, if you saved the projects in C:/defectpred, three of the prjects would be listed as: `ProjectRepository`=C:/defectpred/blender,C:/defectpred/busybox,C:/defectpred/emacs
 6. Change line 8 by specifying the directory where you would like results to be saved, e.g., `AnalysisDirectory`=C:/studyresults. Note that a subdirectory called FBFMetrrics will be created in which the output CSVs will be generated per project e.g., irssi/irssi_metrics_summary.csv for project irssi
 7. Change line 18 by specifying the commit CSV files seperated by commas. Note that the order in which the CSV files are listed MUST match the order of the projects in line 2 (Step 5). Here you point to the CSV files you downloaded which contain the last commit in each release per project.
 8. If you would like to see detailed output for each commit, change line 24 and set `PrintMetricDetails`=true, otherwise leave it as false. Setting this property to false will generate only summary CSVs showing release, commitHash,feature, and their metrics which we import into our analysis scripts. The detailed printout shows features in each file and their metrics but is not used by our analysis scripts.
 9. Open a command window and change the working directory to the location where `FeatRacer` is saved
 10. Execute `FeatRacer` by running the following command: java -jar featracer.jar
 11. A progress bar appears in your command output and will indicate when the calculation is completed
 12. Note that we have provided our output of the metric csv files in the `structure_metrics_files` folder. Each csv file is prefixed with the project name e.g., `blender_metrics_summary.csv` for project blender, and `irssi_metrics_summary.csv` for project irssi

### Finalization of Datasets

The finalization of the datasets is carried out with the script `3-finalization_clean.py` and contains various steps:

1. Finalization of the dataset
2. Import of the structural metrics (SCAT, TANGA, NDEP, LOFC)
3. Split into training and test data
4. Export of datasets (`QUEIROZMET`, `PROCMET`, `PROCSTRUCTMET`) as .arff files

To run the script via Command Line / PowerShell / Terminal use: `python 3-finalization_clean.py`. Depending on the amount of data, the execution of the script may take some time. **Please keep the resulting .csv files from FeatRacer containing the structural metrics data in the dedicated folder and adopt its naming schema.**

**Please note:** Before the script can be executed, configurations must be made in it. These are located in lines 10 to 32, marked with `ENTER_HERE`, divided into three categories and read as follows:

*MySQL-database information*
 
  * `host` - server name or IP adress on which your MySQL-server is running (i.e. *localhost*)
  * `user` - username of MySQL-server (i.e. *root*)
  * `password` - password of MySQL-server
  * `database` - name of the database, which was previously assigned
  * `softwares` - list of names of software projects whose data will be used to create the final data sets

*Split information*

* `train_releases` - list of lists of releases assigned to the final training dataset. Please add a separate "sub-list" for each software project in the list. Keep the order of the `softwares` list.  
* `test_releases` - list of lists of releases assigned to the final test dataset. Please add a separate "sub-list" for each software project in the list. Keep the order of the `softwares` list.  

*Further configurations*

* `keep_main` - decision (True / False) whether main tables should be kept
* `keep_metrics` - decision (True / False) whether metrics tables should be kept
* `keep_final` - decision (True / False) whether final tables should be kept

The resulting .arff files are ready to be used in the WEKA Workbench. They are split into training and test files for each dataset and call as follows:

* `dataset_queirozmet_train.arff` and `dataset_queirozmet_test.arff` with the metrics: FCOMM, FADEV, FDDEV, FEXP, FOEXP
* `dataset_procmet_train.arff` and `dataset_procmet_test.arff` with the metrics: FCOMM, FADEV, FDDEV, FEXP, FOEXP, FMODD, FADDL, FREML
* `dataset_procstructmet_train.arff` and `dataset_procstructmet_test.arff` with the metrics: FCOMM, FADEV, FDDEV, FEXP, FOEXP, FMODD, FADDL, FREML, FNLOC, FCYCO, SCAT, TANGA, NDEP, LOFC

## Templates

In order to be able to fully reproduce the work steps listed in the paper, pre-filled templates (see folder Scipts/Templates) of the scripts explained above were created. They contain all configuration information of the twelve software projects whose data was used. Only the information of the database used must be completed (see explanations of the scripts above).
Run all scripts via Command Line / PowerShell / Terminal in the specified order:

1. `1-processing_PROJECTNAME` for each software project
2. `2-metrics_allprojects`
3. `3-finalization_allprojects`

To execute the first scripts, it is necessary to create a local copy of the repositories. The URLs required for this are specified in the scripts. Due to the large amount of data, the execution of all scripts takes a *long* time. The resulting .arff files are ready to be used in the WEKA Workbench.

## Application of the Datasets

The application of the data sets and thus the execution of the experiments takes place in the graphical interface of the WEKA Workbench. 

Start WEKA and open the "Explorer". Under "Open file..." select one of the training datasets and switch to the tab "Classify". Activate "Supplied test set", click "Set..." > "Open file..." and select the corresponding test data set. If not already done automatically, select "(Nom) label" under "Class". Close the selection window and make sure that the same entry is selected above the "Start" button.

Now select a classifier in the "Classifier" section by clicking "Choose". The following classifiers were used for the paper. The categories of these are given in brackets.

* `J48` (trees) - decision trees
* `IBk` (lazy) - k-nearest-neighbors
* `Logistic` (functions) - logistic regression
* `NaiveBayes` (bayes) - naive bayes
* `MultilayerPerceptron`* (functions) - artificial neural networks
* `RandomForest`* (trees) - random forest
* `SGD` (functions) - stochastic gradient descent
* `SMO` (functions) - support vector machines

\* Additional settings (which we also document in the paper) must be made for these classifiers. To do this, click on the field next to the "Choose" button, which displays the name of the classifier. In case of `MultilayerPerceptron` enter 13,13,13 in the field "hiddenLayers". Replace the value "numIterations" for `RandomForest` with 200. Confirm with "OK".

The experiments can be started with "Start". As soon as the experiment is finished, "OK" is displayed at the end of the window and the results are shown under "Classifier output". To view the ROC curve of the results, right-click on the desired experiment under "Result list" and select "Visualize threshold curve" > "defect". The results of the paper's experiments can be viewed in the "Results" folder.

## Special notice

All contents of the repository were created by Stefan Strüder in 2020. Exceptions are as follows: FeatRacer was developed by Mukelabai Mukelabai as part of an external project. The description for using FeatRacer is by the same author.
Questions or bugs can be sent to the following e-mail address: [contact](mailto:stefanstrueder@uni-koblenz.de).