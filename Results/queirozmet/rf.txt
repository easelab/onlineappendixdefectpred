=== Run information ===

Scheme:       weka.classifiers.trees.RandomForest -P 100 -I 200 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1
Relation:     dataset_queirozmet_train
Instances:    10209
Attributes:   6
              fcomm
              fadev
              fddev
              fexp
              foexp
              label
Test mode:    user supplied test set:  size unknown (reading incrementally)

=== Classifier model (full training set) ===

RandomForest

Bagging with 200 iterations and base learner

weka.classifiers.trees.RandomTree -K 0 -M 1.0 -V 0.001 -S 1 -do-not-check-capabilities

Time taken to build model: 10.89 seconds

=== Evaluation on test set ===

Time taken to test model on supplied test set: 0.55 seconds

=== Summary ===

Correctly Classified Instances        2801               61.8869 %
Incorrectly Classified Instances      1725               38.1131 %
Kappa statistic                          0.1362
Mean absolute error                      0.4285
Root mean squared error                  0.5707
Relative absolute error                135.3504 %
Root relative squared error            147.7502 %
Total Number of Instances             4526     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,574    0,371    0,255      0,574    0,353      0,159    0,587     0,197     defective
                 0,629    0,426    0,870      0,629    0,730      0,159    0,587     0,878     clean
Weighted Avg.    0,619    0,416    0,758      0,619    0,662      0,159    0,587     0,755     

=== Confusion Matrix ===

    a    b   <-- classified as
  470  349 |    a = defective
 1376 2331 |    b = clean

