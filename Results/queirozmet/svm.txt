=== Run information ===

Scheme:       weka.classifiers.functions.SMO -C 1.0 -L 0.001 -P 1.0E-12 -N 0 -V -1 -W 1 -K "weka.classifiers.functions.supportVector.PolyKernel -E 1.0 -C 250007" -calibrator "weka.classifiers.functions.Logistic -R 1.0E-8 -M -1 -num-decimal-places 4"
Relation:     dataset_queirozmet_train
Instances:    10209
Attributes:   6
              fcomm
              fadev
              fddev
              fexp
              foexp
              label
Test mode:    user supplied test set:  size unknown (reading incrementally)

=== Classifier model (full training set) ===

SMO

Kernel used:
  Linear Kernel: K(x,y) = <x,y>

Classifier for classes: defective, clean

BinarySMO

Machine linear: showing attribute weights, not support vectors.

        -6.0636 * (normalized) fcomm
 +     -12.2837 * (normalized) fadev
 +      -1.5583 * (normalized) fddev
 +       0.0245 * (normalized) fexp
 +      -0.0701 * (normalized) foexp
 +       1.0145

Number of kernel evaluations: 534659 (65.233% cached)



Time taken to build model: 0.6 seconds

=== Evaluation on test set ===

Time taken to test model on supplied test set: 0.04 seconds

=== Summary ===

Correctly Classified Instances        3783               83.5837 %
Incorrectly Classified Instances       743               16.4163 %
Kappa statistic                          0.168 
Mean absolute error                      0.1642
Root mean squared error                  0.4052
Relative absolute error                 51.8585 %
Root relative squared error            104.8924 %
Total Number of Instances             4526     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,117    0,005    0,828      0,117    0,205      0,272    0,556     0,257     defective
                 0,995    0,883    0,836      0,995    0,908      0,272    0,556     0,836     clean
Weighted Avg.    0,836    0,724    0,835      0,836    0,781      0,272    0,556     0,731     

=== Confusion Matrix ===

    a    b   <-- classified as
   96  723 |    a = defective
   20 3687 |    b = clean

