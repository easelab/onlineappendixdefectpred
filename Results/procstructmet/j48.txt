=== Run information ===

Scheme:       weka.classifiers.trees.J48 -C 0.25 -M 2
Relation:     dataset_procstructmet_train
Instances:    10209
Attributes:   15
              fcomm
              fadev
              fddev
              fexp
              ffoexp
              fmodd
              fnloc
              fcyco
              faddl
              freml
              scat
              tanga
              ndep
              lofc
              label
Test mode:    user supplied test set:  size unknown (reading incrementally)

=== Classifier model (full training set) ===

J48 pruned tree
------------------

fcomm <= 2
|   faddl <= 8702
|   |   fnloc <= 109
|   |   |   fnloc <= 4: clean (1042.0/4.0)
|   |   |   fnloc > 4
|   |   |   |   foexp <= 126: clean (727.0/23.0)
|   |   |   |   foexp > 126
|   |   |   |   |   fexp <= 6875
|   |   |   |   |   |   fmodd <= 2
|   |   |   |   |   |   |   faddl <= 184
|   |   |   |   |   |   |   |   faddl <= 21: clean (16.0)
|   |   |   |   |   |   |   |   faddl > 21
|   |   |   |   |   |   |   |   |   freml <= 104
|   |   |   |   |   |   |   |   |   |   fcomm <= 1
|   |   |   |   |   |   |   |   |   |   |   cyco <= 23
|   |   |   |   |   |   |   |   |   |   |   |   fexp <= 145: clean (14.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   fexp > 145
|   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 166: defective (11.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 166
|   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 182: clean (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 182: defective (3.0)
|   |   |   |   |   |   |   |   |   |   |   cyco > 23: clean (8.0)
|   |   |   |   |   |   |   |   |   |   fcomm > 1: clean (14.0/1.0)
|   |   |   |   |   |   |   |   |   freml > 104: defective (7.0)
|   |   |   |   |   |   |   faddl > 184: clean (26.0/1.0)
|   |   |   |   |   |   fmodd > 2: clean (22.0/1.0)
|   |   |   |   |   fexp > 6875: clean (33.0)
|   |   fnloc > 109
|   |   |   cyco <= 3476
|   |   |   |   faddl <= 37
|   |   |   |   |   fadev <= 1: clean (2233.0/210.0)
|   |   |   |   |   fadev > 1
|   |   |   |   |   |   fmodd <= 3
|   |   |   |   |   |   |   freml <= 30
|   |   |   |   |   |   |   |   foexp <= 16
|   |   |   |   |   |   |   |   |   faddl <= 10: clean (62.0/5.0)
|   |   |   |   |   |   |   |   |   faddl > 10
|   |   |   |   |   |   |   |   |   |   ndep <= 1
|   |   |   |   |   |   |   |   |   |   |   foexp <= 11
|   |   |   |   |   |   |   |   |   |   |   |   fddev <= 5
|   |   |   |   |   |   |   |   |   |   |   |   |   freml <= 7: defective (8.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   freml > 7: clean (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   fddev > 5: clean (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   foexp > 11: clean (4.0)
|   |   |   |   |   |   |   |   |   |   ndep > 1: clean (4.0)
|   |   |   |   |   |   |   |   foexp > 16
|   |   |   |   |   |   |   |   |   fddev <= 4
|   |   |   |   |   |   |   |   |   |   fmodd <= 2
|   |   |   |   |   |   |   |   |   |   |   foexp <= 76: defective (34.0/14.0)
|   |   |   |   |   |   |   |   |   |   |   foexp > 76: clean (4.0)
|   |   |   |   |   |   |   |   |   |   fmodd > 2
|   |   |   |   |   |   |   |   |   |   |   freml <= 2: clean (2.0)
|   |   |   |   |   |   |   |   |   |   |   freml > 2
|   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 403: clean (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   fnloc > 403: defective (6.0)
|   |   |   |   |   |   |   |   |   fddev > 4
|   |   |   |   |   |   |   |   |   |   fexp <= 567: clean (11.0/1.0)
|   |   |   |   |   |   |   |   |   |   fexp > 567: defective (2.0)
|   |   |   |   |   |   |   freml > 30: clean (20.0)
|   |   |   |   |   |   fmodd > 3: defective (14.0/4.0)
|   |   |   |   faddl > 37
|   |   |   |   |   foexp <= 14512
|   |   |   |   |   |   cyco <= 7
|   |   |   |   |   |   |   faddl <= 109
|   |   |   |   |   |   |   |   faddl <= 97: clean (27.0/4.0)
|   |   |   |   |   |   |   |   faddl > 97: defective (5.0)
|   |   |   |   |   |   |   faddl > 109
|   |   |   |   |   |   |   |   foexp <= 2601: clean (249.0/2.0)
|   |   |   |   |   |   |   |   foexp > 2601
|   |   |   |   |   |   |   |   |   foexp <= 4265: defective (7.0/1.0)
|   |   |   |   |   |   |   |   |   foexp > 4265: clean (15.0)
|   |   |   |   |   |   cyco > 7
|   |   |   |   |   |   |   fexp <= 735124
|   |   |   |   |   |   |   |   fexp <= 143658
|   |   |   |   |   |   |   |   |   faddl <= 556
|   |   |   |   |   |   |   |   |   |   fmodd <= 1
|   |   |   |   |   |   |   |   |   |   |   freml <= 104
|   |   |   |   |   |   |   |   |   |   |   |   faddl <= 118
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 173
|   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 75
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 5720: clean (11.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 5720: defective (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 75: defective (17.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 173: clean (358.0/80.0)
|   |   |   |   |   |   |   |   |   |   |   |   faddl > 118
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 131: clean (14.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 131
|   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev <= 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 150: defective (13.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 150
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 2354
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco <= 49
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 305: clean (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 305
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 361: defective (8.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 361
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 412: clean (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 412: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco > 49
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 506: clean (46.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 506
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 334: clean (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 334: defective (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 2354
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 3550: defective (9.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 3550
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 4806
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 18828
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml <= 47
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 423: clean (22.0/3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 423: defective (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml > 47
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco <= 185: defective (11.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco > 185
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml <= 62: defective (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml > 62
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat <= 0: clean (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 14400: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 14400: clean (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 18828: clean (15.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 4806: defective (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev > 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat <= 7
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 320
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev <= 2: defective (13.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev > 2
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco <= 175: defective (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco > 175: clean (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 320: defective (9.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat > 7: defective (8.0)
|   |   |   |   |   |   |   |   |   |   |   freml > 104
|   |   |   |   |   |   |   |   |   |   |   |   faddl <= 272
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 908: clean (124.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 908
|   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco <= 167: defective (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco > 167
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat <= 3: clean (48.0/5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat > 3
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml <= 183: clean (12.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml > 183: defective (7.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   faddl > 272
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 555: defective (10.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 555
|   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 3349
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 43836: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 43836: clean (14.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 3349: defective (5.0)
|   |   |   |   |   |   |   |   |   |   fmodd > 1
|   |   |   |   |   |   |   |   |   |   |   fexp <= 60192
|   |   |   |   |   |   |   |   |   |   |   |   faddl <= 56
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 2295
|   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 41
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 11: clean (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 11: defective (14.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 41
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 43: clean (8.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 43
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fcomm <= 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco <= 356: clean (11.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco > 356: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fcomm > 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 1371
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev <= 1: defective (8.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev > 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat <= 1: clean (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat > 1: defective (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 1371: clean (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 2295
|   |   |   |   |   |   |   |   |   |   |   |   |   |   freml <= 6: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   freml > 6: clean (38.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   faddl > 56
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 402
|   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd <= 2
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml <= 7
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco <= 43: defective (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco > 43: clean (9.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml > 7
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 18438: defective (70.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 18438
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 305: clean (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 305: defective (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd > 2
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 544: defective (22.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 544
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev <= 5: clean (18.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev > 5: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 402
|   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd <= 3
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fcomm <= 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd <= 2
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 3025
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev <= 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 867
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 151: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 151: clean (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 867: defective (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev > 1: defective (8.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 3025: clean (24.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd > 2
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 95: defective (7.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 95: clean (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fcomm > 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   ndep <= 3
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 30723
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 1215
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev <= 4
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 122
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd <= 2
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fadev <= 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat <= 27
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 4507
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   ndep <= 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev <= 3
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 3520
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev <= 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco <= 184
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 147: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 147
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco <= 137: clean (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco > 137
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 277: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 277: clean (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco > 184: defective (8.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev > 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 1570: defective (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 1570: clean (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 3520: clean (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev > 3: clean (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   ndep > 1: defective (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 4507: defective (13.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat > 27: clean (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fadev > 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   ndep <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml <= 46
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 158: clean (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 158: defective (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml > 46: clean (14.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   ndep > 0: defective (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd > 2
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat <= 5: clean (7.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat > 5: defective (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 122
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 1208
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 539: defective (12.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 539
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat <= 2
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml <= 28: defective (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml > 28: clean (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   scat > 2: clean (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 1208: defective (19.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev > 4
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   ndep <= 0: clean (16.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   ndep > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev <= 7: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fddev > 7: clean (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 1215: clean (9.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 30723: defective (11.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   ndep > 3: defective (11.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd > 3
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd <= 4: defective (18.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fmodd > 4
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp <= 216: defective (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fexp > 216: clean (5.0)
|   |   |   |   |   |   |   |   |   |   |   fexp > 60192
|   |   |   |   |   |   |   |   |   |   |   |   fddev <= 11: clean (26.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   fddev > 11: defective (2.0)
|   |   |   |   |   |   |   |   |   faddl > 556
|   |   |   |   |   |   |   |   |   |   foexp <= 765: defective (77.0/1.0)
|   |   |   |   |   |   |   |   |   |   foexp > 765
|   |   |   |   |   |   |   |   |   |   |   fnloc <= 735
|   |   |   |   |   |   |   |   |   |   |   |   fcomm <= 1: clean (38.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   fcomm > 1: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   fnloc > 735
|   |   |   |   |   |   |   |   |   |   |   |   freml <= 739
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 1163: defective (44.0/5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 1163
|   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 2629: clean (18.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 2629
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 4279: defective (14.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 4279
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp <= 6368: clean (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   foexp > 6368: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   freml > 739: defective (17.0)
|   |   |   |   |   |   |   |   fexp > 143658
|   |   |   |   |   |   |   |   |   fexp <= 164088: defective (40.0)
|   |   |   |   |   |   |   |   |   fexp > 164088
|   |   |   |   |   |   |   |   |   |   cyco <= 55: clean (5.0)
|   |   |   |   |   |   |   |   |   |   cyco > 55
|   |   |   |   |   |   |   |   |   |   |   fadev <= 1
|   |   |   |   |   |   |   |   |   |   |   |   fexp <= 236989: defective (17.0)
|   |   |   |   |   |   |   |   |   |   |   |   fexp > 236989
|   |   |   |   |   |   |   |   |   |   |   |   |   faddl <= 795
|   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco <= 644: clean (10.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   cyco > 644: defective (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   faddl > 795: defective (10.0)
|   |   |   |   |   |   |   |   |   |   |   fadev > 1: defective (11.0)
|   |   |   |   |   |   |   fexp > 735124
|   |   |   |   |   |   |   |   fnloc <= 3071: clean (116.0)
|   |   |   |   |   |   |   |   fnloc > 3071
|   |   |   |   |   |   |   |   |   fadev <= 1: defective (12.0)
|   |   |   |   |   |   |   |   |   fadev > 1: clean (4.0)
|   |   |   |   |   foexp > 14512: defective (58.0/1.0)
|   |   |   cyco > 3476
|   |   |   |   scat <= 4: clean (720.0)
|   |   |   |   scat > 4
|   |   |   |   |   ndep <= 10: clean (23.0/1.0)
|   |   |   |   |   ndep > 10: defective (2.0)
|   faddl > 8702: clean (1371.0)
fcomm > 2
|   foexp <= 2956
|   |   fcomm <= 6
|   |   |   faddl <= 18
|   |   |   |   fmodd <= 4
|   |   |   |   |   scat <= 21
|   |   |   |   |   |   fmodd <= 3: clean (96.0/18.0)
|   |   |   |   |   |   fmodd > 3
|   |   |   |   |   |   |   fcomm <= 3
|   |   |   |   |   |   |   |   freml <= 12
|   |   |   |   |   |   |   |   |   freml <= 1: clean (3.0)
|   |   |   |   |   |   |   |   |   freml > 1
|   |   |   |   |   |   |   |   |   |   fddev <= 2: clean (5.0/2.0)
|   |   |   |   |   |   |   |   |   |   fddev > 2: defective (6.0)
|   |   |   |   |   |   |   |   freml > 12: clean (8.0)
|   |   |   |   |   |   |   fcomm > 3
|   |   |   |   |   |   |   |   foexp <= 67
|   |   |   |   |   |   |   |   |   fnloc <= 350
|   |   |   |   |   |   |   |   |   |   fnloc <= 114: clean (3.0)
|   |   |   |   |   |   |   |   |   |   fnloc > 114: defective (3.0)
|   |   |   |   |   |   |   |   |   fnloc > 350: clean (14.0)
|   |   |   |   |   |   |   |   foexp > 67
|   |   |   |   |   |   |   |   |   foexp <= 82: defective (5.0)
|   |   |   |   |   |   |   |   |   foexp > 82
|   |   |   |   |   |   |   |   |   |   fexp <= 91: clean (2.0)
|   |   |   |   |   |   |   |   |   |   fexp > 91: defective (2.0)
|   |   |   |   |   scat > 21: clean (43.0/1.0)
|   |   |   |   fmodd > 4
|   |   |   |   |   faddl <= 6
|   |   |   |   |   |   fcomm <= 5
|   |   |   |   |   |   |   fnloc <= 1067: clean (19.0/1.0)
|   |   |   |   |   |   |   fnloc > 1067
|   |   |   |   |   |   |   |   fadev <= 1
|   |   |   |   |   |   |   |   |   scat <= 0
|   |   |   |   |   |   |   |   |   |   foexp <= 390: defective (2.0)
|   |   |   |   |   |   |   |   |   |   foexp > 390: clean (3.0/1.0)
|   |   |   |   |   |   |   |   |   scat > 0: clean (7.0)
|   |   |   |   |   |   |   |   fadev > 1
|   |   |   |   |   |   |   |   |   scat <= 111: defective (8.0/1.0)
|   |   |   |   |   |   |   |   |   scat > 111: clean (2.0)
|   |   |   |   |   |   fcomm > 5: clean (9.0)
|   |   |   |   |   faddl > 6
|   |   |   |   |   |   fadev <= 2
|   |   |   |   |   |   |   fadev <= 1
|   |   |   |   |   |   |   |   ndep <= 0: clean (26.0/6.0)
|   |   |   |   |   |   |   |   ndep > 0
|   |   |   |   |   |   |   |   |   faddl <= 15
|   |   |   |   |   |   |   |   |   |   fddev <= 3
|   |   |   |   |   |   |   |   |   |   |   fcomm <= 4: defective (3.0)
|   |   |   |   |   |   |   |   |   |   |   fcomm > 4: clean (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   fddev > 3: defective (4.0)
|   |   |   |   |   |   |   |   |   faddl > 15: clean (6.0/1.0)
|   |   |   |   |   |   |   fadev > 1
|   |   |   |   |   |   |   |   scat <= 24
|   |   |   |   |   |   |   |   |   ndep <= 0
|   |   |   |   |   |   |   |   |   |   freml <= 9: clean (4.0)
|   |   |   |   |   |   |   |   |   |   freml > 9
|   |   |   |   |   |   |   |   |   |   |   fcomm <= 4
|   |   |   |   |   |   |   |   |   |   |   |   scat <= 1: clean (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   scat > 1: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   fcomm > 4: defective (6.0)
|   |   |   |   |   |   |   |   |   ndep > 0: defective (3.0)
|   |   |   |   |   |   |   |   scat > 24: clean (9.0/1.0)
|   |   |   |   |   |   fadev > 2
|   |   |   |   |   |   |   fmodd <= 6
|   |   |   |   |   |   |   |   cyco <= 63: clean (6.0)
|   |   |   |   |   |   |   |   cyco > 63
|   |   |   |   |   |   |   |   |   scat <= 62: defective (6.0/1.0)
|   |   |   |   |   |   |   |   |   scat > 62: clean (2.0)
|   |   |   |   |   |   |   fmodd > 6: defective (13.0/1.0)
|   |   |   faddl > 18
|   |   |   |   fadev <= 1
|   |   |   |   |   faddl <= 64
|   |   |   |   |   |   ndep <= 0
|   |   |   |   |   |   |   cyco <= 973
|   |   |   |   |   |   |   |   fcomm <= 3
|   |   |   |   |   |   |   |   |   scat <= 4
|   |   |   |   |   |   |   |   |   |   fmodd <= 3
|   |   |   |   |   |   |   |   |   |   |   fddev <= 3
|   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 1267
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc <= 8: defective (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   fnloc > 8: clean (14.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   fnloc > 1267: defective (4.0)
|   |   |   |   |   |   |   |   |   |   |   fddev > 3: defective (3.0)
|   |   |   |   |   |   |   |   |   |   fmodd > 3
|   |   |   |   |   |   |   |   |   |   |   scat <= 0: clean (12.0)
|   |   |   |   |   |   |   |   |   |   |   scat > 0: defective (14.0/1.0)
|   |   |   |   |   |   |   |   |   scat > 4: defective (4.0)
|   |   |   |   |   |   |   |   fcomm > 3: defective (31.0/12.0)
|   |   |   |   |   |   |   cyco > 973: clean (17.0/1.0)
|   |   |   |   |   |   ndep > 0: clean (55.0/11.0)
|   |   |   |   |   faddl > 64: defective (127.0/43.0)
|   |   |   |   fadev > 1
|   |   |   |   |   cyco <= 29
|   |   |   |   |   |   fexp <= 560
|   |   |   |   |   |   |   fnloc <= 0: defective (13.0/1.0)
|   |   |   |   |   |   |   fnloc > 0
|   |   |   |   |   |   |   |   fadev <= 2: clean (19.0/5.0)
|   |   |   |   |   |   |   |   fadev > 2
|   |   |   |   |   |   |   |   |   fexp <= 74: clean (4.0/1.0)
|   |   |   |   |   |   |   |   |   fexp > 74: defective (9.0/2.0)
|   |   |   |   |   |   fexp > 560: clean (14.0/1.0)
|   |   |   |   |   cyco > 29
|   |   |   |   |   |   fmodd <= 3
|   |   |   |   |   |   |   freml <= 5: clean (7.0)
|   |   |   |   |   |   |   freml > 5
|   |   |   |   |   |   |   |   fnloc <= 4121
|   |   |   |   |   |   |   |   |   fnloc <= 452: defective (18.0/2.0)
|   |   |   |   |   |   |   |   |   fnloc > 452
|   |   |   |   |   |   |   |   |   |   fadev <= 2
|   |   |   |   |   |   |   |   |   |   |   faddl <= 424: clean (28.0/10.0)
|   |   |   |   |   |   |   |   |   |   |   faddl > 424: defective (4.0)
|   |   |   |   |   |   |   |   |   |   fadev > 2
|   |   |   |   |   |   |   |   |   |   |   faddl <= 61: clean (7.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   faddl > 61: defective (6.0/1.0)
|   |   |   |   |   |   |   |   fnloc > 4121: defective (12.0)
|   |   |   |   |   |   fmodd > 3
|   |   |   |   |   |   |   fexp <= 3721
|   |   |   |   |   |   |   |   fcomm <= 4
|   |   |   |   |   |   |   |   |   fcomm <= 3
|   |   |   |   |   |   |   |   |   |   fadev <= 2: defective (20.0/2.0)
|   |   |   |   |   |   |   |   |   |   fadev > 2
|   |   |   |   |   |   |   |   |   |   |   foexp <= 253: defective (16.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   foexp > 253: clean (3.0)
|   |   |   |   |   |   |   |   |   fcomm > 3
|   |   |   |   |   |   |   |   |   |   fmodd <= 7
|   |   |   |   |   |   |   |   |   |   |   fmodd <= 5
|   |   |   |   |   |   |   |   |   |   |   |   fexp <= 34: clean (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   fexp > 34
|   |   |   |   |   |   |   |   |   |   |   |   |   fmodd <= 4
|   |   |   |   |   |   |   |   |   |   |   |   |   |   lofc <= 79
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml <= 36: defective (8.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   freml > 36
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fadev <= 3: clean (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   fadev > 3: defective (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   lofc > 79: defective (13.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   fmodd > 4: defective (11.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   fmodd > 5
|   |   |   |   |   |   |   |   |   |   |   |   ndep <= 0: clean (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   ndep > 0: defective (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   fmodd > 7: defective (7.0)
|   |   |   |   |   |   |   |   fcomm > 4
|   |   |   |   |   |   |   |   |   fcomm <= 5
|   |   |   |   |   |   |   |   |   |   fexp <= 303: defective (36.0/3.0)
|   |   |   |   |   |   |   |   |   |   fexp > 303
|   |   |   |   |   |   |   |   |   |   |   fexp <= 1022: clean (8.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   fexp > 1022: defective (9.0)
|   |   |   |   |   |   |   |   |   fcomm > 5: defective (37.0/4.0)
|   |   |   |   |   |   |   fexp > 3721: defective (29.0)
|   |   fcomm > 6
|   |   |   fadev <= 1
|   |   |   |   fddev <= 3
|   |   |   |   |   faddl <= 39
|   |   |   |   |   |   cyco <= 1534
|   |   |   |   |   |   |   ndep <= 4
|   |   |   |   |   |   |   |   ndep <= 1
|   |   |   |   |   |   |   |   |   fmodd <= 10: defective (7.0)
|   |   |   |   |   |   |   |   |   fmodd > 10
|   |   |   |   |   |   |   |   |   |   fcomm <= 7: clean (3.0)
|   |   |   |   |   |   |   |   |   |   fcomm > 7
|   |   |   |   |   |   |   |   |   |   |   freml <= 7: defective (6.0)
|   |   |   |   |   |   |   |   |   |   |   freml > 7
|   |   |   |   |   |   |   |   |   |   |   |   foexp <= 909: clean (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   foexp > 909: defective (3.0/1.0)
|   |   |   |   |   |   |   |   ndep > 1: clean (2.0)
|   |   |   |   |   |   |   ndep > 4: defective (4.0)
|   |   |   |   |   |   cyco > 1534: clean (4.0)
|   |   |   |   |   faddl > 39
|   |   |   |   |   |   scat <= 2
|   |   |   |   |   |   |   fddev <= 1: defective (2.0)
|   |   |   |   |   |   |   fddev > 1
|   |   |   |   |   |   |   |   fexp <= 25: defective (3.0)
|   |   |   |   |   |   |   |   fexp > 25: clean (3.0)
|   |   |   |   |   |   scat > 2: defective (26.0)
|   |   |   |   fddev > 3
|   |   |   |   |   cyco <= 1382
|   |   |   |   |   |   fcomm <= 7: clean (6.0/2.0)
|   |   |   |   |   |   fcomm > 7: defective (5.0)
|   |   |   |   |   cyco > 1382: clean (10.0)
|   |   |   fadev > 1
|   |   |   |   fmodd <= 14
|   |   |   |   |   fexp <= 39
|   |   |   |   |   |   fcomm <= 8
|   |   |   |   |   |   |   fcomm <= 7
|   |   |   |   |   |   |   |   freml <= 5: defective (2.0)
|   |   |   |   |   |   |   |   freml > 5: clean (6.0)
|   |   |   |   |   |   |   fcomm > 7
|   |   |   |   |   |   |   |   faddl <= 13
|   |   |   |   |   |   |   |   |   faddl <= 6: defective (2.0)
|   |   |   |   |   |   |   |   |   faddl > 6: clean (6.0)
|   |   |   |   |   |   |   |   faddl > 13: defective (4.0)
|   |   |   |   |   |   fcomm > 8
|   |   |   |   |   |   |   fmodd <= 13
|   |   |   |   |   |   |   |   fexp <= 9: clean (3.0/1.0)
|   |   |   |   |   |   |   |   fexp > 9: defective (11.0)
|   |   |   |   |   |   |   fmodd > 13: clean (3.0/1.0)
|   |   |   |   |   fexp > 39: defective (97.0/9.0)
|   |   |   |   fmodd > 14: defective (120.0/2.0)
|   foexp > 2956
|   |   cyco <= 82
|   |   |   fddev <= 3
|   |   |   |   scat <= 2: clean (6.0)
|   |   |   |   scat > 2: defective (5.0/1.0)
|   |   |   fddev > 3: defective (5.0)
|   |   cyco > 82
|   |   |   faddl <= 54
|   |   |   |   fcomm <= 8: clean (4.0)
|   |   |   |   fcomm > 8: defective (8.0)
|   |   |   faddl > 54: defective (226.0/3.0)

Number of Leaves  : 	260

Size of the tree : 	519


Time taken to build model: 0.46 seconds

=== Evaluation on test set ===

Time taken to test model on supplied test set: 0.03 seconds

=== Summary ===

Correctly Classified Instances        3678               81.2638 %
Incorrectly Classified Instances       848               18.7362 %
Kappa statistic                          0.4389
Mean absolute error                      0.2153
Root mean squared error                  0.3987
Relative absolute error                 68.024  %
Root relative squared error            103.2122 %
Total Number of Instances             4526     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,645    0,150    0,487      0,645    0,555      0,446    0,784     0,421     defective
                 0,850    0,355    0,915      0,850    0,881      0,446    0,784     0,924     clean
Weighted Avg.    0,813    0,318    0,838      0,813    0,822      0,446    0,784     0,833     

=== Confusion Matrix ===

    a    b   <-- classified as
  528  291 |    a = defective
  557 3150 |    b = clean

