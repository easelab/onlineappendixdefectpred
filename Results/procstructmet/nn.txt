=== Run information ===

Scheme:       weka.classifiers.functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 2000 -V 0 -S 0 -E 20 -H "13, 13, 13"
Relation:     dataset_procstructmet_train
Instances:    10209
Attributes:   15
              fcomm
              fadev
              fddev
              fexp
              foexp
              fmodd
              fnloc
              fcyco
              faddl
              freml
              scat
              tanga
              ndep
              lofc
              label
Test mode:    user supplied test set:  size unknown (reading incrementally)

=== Classifier model (full training set) ===

Sigmoid Node 0
    Inputs    Weights
    Threshold    2.971946174623879
    Node 28    -1.125514967477754
    Node 29    -1.2288211911165514
    Node 30    -1.4170995364048782
    Node 31    -2.080449029825753
    Node 32    -1.2833880661056762
    Node 33    -1.114088551489303
    Node 34    -2.2196392342063187
    Node 35    -2.6413900435298188
    Node 36    -1.1280808410134497
    Node 37    -1.2195689099200195
    Node 38    -1.0774616259370111
    Node 39    -1.1904855233160643
    Node 40    -1.1124007055174472
Sigmoid Node 1
    Inputs    Weights
    Threshold    -2.9717908797568207
    Node 28    1.1155012798139161
    Node 29    1.2810690491270469
    Node 30    1.379978411459953
    Node 31    2.1311420031484754
    Node 32    1.2812163266063659
    Node 33    1.102850901683181
    Node 34    2.174133119366235
    Node 35    2.641344839971061
    Node 36    1.142662354585274
    Node 37    1.1746344393518349
    Node 38    1.11408505719138
    Node 39    1.199013408860839
    Node 40    1.0961087822259772
Sigmoid Node 2
    Inputs    Weights
    Threshold    -8.599019165347281
    Attrib fcomm    1.476959380224331
    Attrib fadev    5.567786327760634
    Attrib fddev    5.480759474562604
    Attrib fexp    10.187767818955392
    Attrib foexp    -20.189770831817565
    Attrib fmodd    4.062299423007497
    Attrib fnloc    -22.44621848240914
    Attrib fcyco    3.937655795794766
    Attrib faddl    -14.279020055955682
    Attrib freml    -2.7071000194260146
    Attrib scat    -5.224368255690712
    Attrib tanga    9.06197537167595
    Attrib ndep    3.65213418022003
    Attrib lofc    5.971460020756318
Sigmoid Node 3
    Inputs    Weights
    Threshold    5.928774843357489
    Attrib fcomm    1.2157404200646489
    Attrib fadev    7.150542965562985
    Attrib fddev    0.9469392716558361
    Attrib fexp    -4.645334348288693
    Attrib foexp    6.231739706515809
    Attrib fmodd    -0.7520265846797191
    Attrib fnloc    2.141573418792803
    Attrib fcyco    -0.6600656469332014
    Attrib faddl    -2.899108578882798
    Attrib freml    -3.4640868905424034
    Attrib scat    1.837136823107793
    Attrib tanga    -5.284266269774624
    Attrib ndep    2.1030897286465087
    Attrib lofc    6.214655310651016
Sigmoid Node 4
    Inputs    Weights
    Threshold    2.7022147692673095
    Attrib fcomm    1.4243506073936278
    Attrib fadev    4.591687096894285
    Attrib fddev    -0.4350018183958073
    Attrib fexp    2.9390565330837566
    Attrib foexp    12.775160572659336
    Attrib fmodd    -0.16619187553379486
    Attrib fnloc    -1.1734243024978637
    Attrib fcyco    -20.337300304344073
    Attrib faddl    2.0342761130756717
    Attrib freml    7.574057153848225
    Attrib scat    1.3748910836246837
    Attrib tanga    -2.9821209156732036
    Attrib ndep    -2.3405288201484753
    Attrib lofc    -3.2463731958522155
Sigmoid Node 5
    Inputs    Weights
    Threshold    1.3909843010430254
    Attrib fcomm    2.1184835815239222
    Attrib fadev    0.7945551529184639
    Attrib fddev    -0.4207466629768902
    Attrib fexp    0.1721846648953922
    Attrib foexp    7.848372199333394
    Attrib fmodd    0.9047806993503966
    Attrib fnloc    0.05198051703717372
    Attrib fcyco    -9.319648581878333
    Attrib faddl    0.028246736852363456
    Attrib freml    0.9053309519561946
    Attrib scat    -1.30791239797338
    Attrib tanga    -1.0464141664843545
    Attrib ndep    -2.1405309585718983
    Attrib lofc    -2.2938943708732227
Sigmoid Node 6
    Inputs    Weights
    Threshold    20.70071961605087
    Attrib fcomm    8.098953192041577
    Attrib fadev    38.55624374389536
    Attrib fddev    11.619116744329972
    Attrib fexp    -18.753691371572625
    Attrib foexp    25.089470341712673
    Attrib fmodd    1.6615463180738952
    Attrib fnloc    1.3716079102956342
    Attrib fcyco    -3.674856354817247
    Attrib faddl    -1.9624868516187046
    Attrib freml    -20.713787076733638
    Attrib scat    10.1088454668197
    Attrib tanga    -20.294208990076676
    Attrib ndep    0.4073164858036644
    Attrib lofc    -8.0056713252309
Sigmoid Node 7
    Inputs    Weights
    Threshold    0.4457070675825028
    Attrib fcomm    1.8636875877657284
    Attrib fadev    0.8011883235503243
    Attrib fddev    -0.36608040206880893
    Attrib fexp    0.6281126306355955
    Attrib foexp    5.721433023453365
    Attrib fmodd    1.0444946816753546
    Attrib fnloc    -2.1060505767808
    Attrib fcyco    -8.0005060086603
    Attrib faddl    -1.2814946632487674
    Attrib freml    1.3659037061487933
    Attrib scat    -2.3427814896990067
    Attrib tanga    -0.43341935797867287
    Attrib ndep    -1.7285613144451497
    Attrib lofc    -0.9412618663005923
Sigmoid Node 8
    Inputs    Weights
    Threshold    22.00122788776898
    Attrib fcomm    4.912871605037563
    Attrib fadev    21.357232908205876
    Attrib fddev    5.355655202152696
    Attrib fexp    -1.9853690424024002
    Attrib foexp    39.57221837885885
    Attrib fmodd    -2.3646598457736565
    Attrib fnloc    5.0956721967373815
    Attrib fcyco    -1.2746575953312325
    Attrib faddl    8.236775934867456
    Attrib freml    -0.6061713526499042
    Attrib scat    -1.3371430205904526
    Attrib tanga    -21.662987784162414
    Attrib ndep    -11.867137687980426
    Attrib lofc    -15.46197022034356
Sigmoid Node 9
    Inputs    Weights
    Threshold    0.9548169602226959
    Attrib fcomm    -0.00885063090292133
    Attrib fadev    -0.2600282637608292
    Attrib fddev    -0.614868750731832
    Attrib fexp    -0.6409981400115466
    Attrib foexp    2.3573093971808436
    Attrib fmodd    -0.39518114846421065
    Attrib fnloc    -4.214670301561666
    Attrib fcyco    -2.9843453691002764
    Attrib faddl    -1.4840671875466345
    Attrib freml    -0.23356289930075774
    Attrib scat    -1.6474668425427375
    Attrib tanga    -0.8674404643772023
    Attrib ndep    -1.4062803273548605
    Attrib lofc    -0.9421192752237172
Sigmoid Node 10
    Inputs    Weights
    Threshold    9.796370741619914
    Attrib fcomm    1.2900526711036944
    Attrib fadev    6.078868565087629
    Attrib fddev    0.9386853350257294
    Attrib fexp    -4.021316183326913
    Attrib foexp    23.386505347982087
    Attrib fmodd    -2.069687181084876
    Attrib fnloc    3.1844285851665686
    Attrib fcyco    -10.245490248532679
    Attrib faddl    8.291130490250408
    Attrib freml    2.4619849732378345
    Attrib scat    1.0273626030135914
    Attrib tanga    -10.063133227124666
    Attrib ndep    -2.161150276064279
    Attrib lofc    -5.495242392725525
Sigmoid Node 11
    Inputs    Weights
    Threshold    20.21500253194154
    Attrib fcomm    2.2241319367178662
    Attrib fadev    27.664572471677303
    Attrib fddev    8.525474654250086
    Attrib fexp    16.751300641643716
    Attrib foexp    6.9434706752850515
    Attrib fmodd    -1.3582427904479566
    Attrib fnloc    -3.0511773372587507
    Attrib fcyco    48.77839358025535
    Attrib faddl    -4.64629194363818
    Attrib freml    -29.01677779934075
    Attrib scat    -5.765550262202949
    Attrib tanga    -19.930172967445557
    Attrib ndep    -12.991716715925554
    Attrib lofc    -10.800859119148457
Sigmoid Node 12
    Inputs    Weights
    Threshold    6.02274243102318
    Attrib fcomm    1.3499454447594776
    Attrib fadev    7.799488661319908
    Attrib fddev    3.018662715344728
    Attrib fexp    -4.586015603871171
    Attrib foexp    8.368453890888246
    Attrib fmodd    -0.6734074722392364
    Attrib fnloc    3.5723379758928107
    Attrib fcyco    -0.7048794060192104
    Attrib faddl    -4.1062705511893745
    Attrib freml    -2.01806326647923
    Attrib scat    -2.5510586677838787
    Attrib tanga    -5.3077321086611144
    Attrib ndep    3.3450919373667687
    Attrib lofc    3.3123579813985597
Sigmoid Node 13
    Inputs    Weights
    Threshold    6.920607978959763
    Attrib fcomm    2.1553133185376527
    Attrib fadev    10.48540470907035
    Attrib fddev    1.6665897210676612
    Attrib fexp    -5.662101947381345
    Attrib foexp    6.2460011997700535
    Attrib fmodd    -0.19660348078055345
    Attrib fnloc    1.6953419395876685
    Attrib fcyco    -0.8043404743936966
    Attrib faddl    -2.8262999552033485
    Attrib freml    -4.923868795968182
    Attrib scat    1.1732420510393766
    Attrib tanga    -6.032242591119104
    Attrib ndep    1.7076489506449959
    Attrib lofc    7.192341577254564
Sigmoid Node 14
    Inputs    Weights
    Threshold    0.4491675871052556
    Attrib fcomm    2.651312965550235
    Attrib fadev    0.28264145472391794
    Attrib fddev    -1.1444318980774346
    Attrib fexp    -0.9457159954708352
    Attrib foexp    6.339405491480406
    Attrib fmodd    1.3587388076526727
    Attrib fnloc    -7.718886456647735
    Attrib fcyco    -9.875956489830086
    Attrib faddl    -3.315155345849832
    Attrib freml    0.1996786137298031
    Attrib scat    -3.157440096340351
    Attrib tanga    -0.38457040317915214
    Attrib ndep    -2.22366232852482
    Attrib lofc    -0.31911798907815586
Sigmoid Node 15
    Inputs    Weights
    Threshold    -9.504075654393679
    Node 2    -5.305636472484665
    Node 3    1.4042639230218246
    Node 4    -0.3597997494384975
    Node 5    1.2281033056967496
    Node 6    0.3404096326361647
    Node 7    1.6422202811618551
    Node 8    4.65074252222617
    Node 9    -6.580262651452912
    Node 10    0.7199008649627818
    Node 11    9.616192880025729
    Node 12    1.8340131938286481
    Node 13    2.3672042674293707
    Node 14    2.234214261298551
Sigmoid Node 16
    Inputs    Weights
    Threshold    -6.748215257112904
    Node 2    2.7247754790213197
    Node 3    0.8764450786324223
    Node 4    -0.40511846732977436
    Node 5    -0.15642676721191925
    Node 6    4.794040826038833
    Node 7    -0.24025218327346787
    Node 8    2.7842997429992935
    Node 9    -3.0763762666225563
    Node 10    -0.28213288657402896
    Node 11    1.7762061564918838
    Node 12    1.8994125598884142
    Node 13    1.7964671459154136
    Node 14    0.17831535635949627
Sigmoid Node 17
    Inputs    Weights
    Threshold    -2.6840018627377553
    Node 2    7.249725033185852
    Node 3    2.1320828733772945
    Node 4    -5.59270102197405
    Node 5    0.8351843940738445
    Node 6    3.3355450974851952
    Node 7    1.5716906121271748
    Node 8    -5.26960553279014
    Node 9    1.2546476113347127
    Node 10    -6.173305355392774
    Node 11    2.7802319190520746
    Node 12    1.3131131982607944
    Node 13    2.0296439736727
    Node 14    6.539471715618661
Sigmoid Node 18
    Inputs    Weights
    Threshold    -7.612288765648267
    Node 2    -4.817196090050943
    Node 3    3.9649598178848318
    Node 4    -1.8498986196438933
    Node 5    1.7309171845242932
    Node 6    -2.2659989855737783
    Node 7    1.1918578593550975
    Node 8    3.601070607448112
    Node 9    -4.244112244324948
    Node 10    3.362305544655771
    Node 11    4.355334209880757
    Node 12    4.4242458922095125
    Node 13    4.592263592425344
    Node 14    0.28980044428136126
Sigmoid Node 19
    Inputs    Weights
    Threshold    -9.499386496759161
    Node 2    -3.238740265739304
    Node 3    3.9374547614658004
    Node 4    -0.8374123624992623
    Node 5    1.680512515184219
    Node 6    5.80567823376326
    Node 7    2.7363884450200167
    Node 8    2.8347243375849427
    Node 9    -5.98965951651637
    Node 10    2.637507470667766
    Node 11    7.4666856180256165
    Node 12    2.8271747477384346
    Node 13    5.49087646717813
    Node 14    1.9684620184777437
Sigmoid Node 20
    Inputs    Weights
    Threshold    -5.771286968562877
    Node 2    -0.30437940224897014
    Node 3    1.5638725627455936
    Node 4    -2.6249166185108472
    Node 5    -0.41524001939088234
    Node 6    3.077127779938076
    Node 7    -0.7450977385527051
    Node 8    3.9424620332278164
    Node 9    -2.1056412954250896
    Node 10    -1.851318059325957
    Node 11    0.8523135204202592
    Node 12    1.8131405587898244
    Node 13    2.040032160201259
    Node 14    0.4256507572514125
Sigmoid Node 21
    Inputs    Weights
    Threshold    -5.034315154987477
    Node 2    3.78741982895774
    Node 3    0.6181881516614308
    Node 4    -4.909786309959721
    Node 5    -0.008265958823604846
    Node 6    1.1757423659707162
    Node 7    0.37414406724231186
    Node 8    2.2420470743853853
    Node 9    -1.2370980493858554
    Node 10    -0.747209871973716
    Node 11    9.285424959131799
    Node 12    0.8582747481583316
    Node 13    1.0412392766756886
    Node 14    2.593358966396747
Sigmoid Node 22
    Inputs    Weights
    Threshold    -7.52975199019376
    Node 2    -2.4869835404686897
    Node 3    1.9193270336084816
    Node 4    2.5314228892033683
    Node 5    3.5638619016468063
    Node 6    3.6517157938938594
    Node 7    2.621446779695609
    Node 8    5.176472091454822
    Node 9    -4.080218960649528
    Node 10    5.1487194273796915
    Node 11    3.2072518670907053
    Node 12    2.9835654713740554
    Node 13    2.9138507208718236
    Node 14    1.326175383230373
Sigmoid Node 23
    Inputs    Weights
    Threshold    -5.52160078316242
    Node 2    -0.4267420039121591
    Node 3    2.7401921522097745
    Node 4    -2.3495694453790597
    Node 5    -0.29030973443810726
    Node 6    3.778046089293651
    Node 7    -0.7091944391464129
    Node 8    3.0931611996406065
    Node 9    -1.850145543059929
    Node 10    -2.0950123513340673
    Node 11    -0.6367601195411795
    Node 12    2.576126208033641
    Node 13    3.3056823343691843
    Node 14    0.6042925262479321
Sigmoid Node 24
    Inputs    Weights
    Threshold    -7.884116428067778
    Node 2    -6.578747866261419
    Node 3    2.374186240950845
    Node 4    1.3003859923879038
    Node 5    0.9811074214645642
    Node 6    6.954276499933356
    Node 7    2.11659139648237
    Node 8    6.906641576672007
    Node 9    -4.402460502150234
    Node 10    3.849016572652673
    Node 11    5.648892940113278
    Node 12    2.329141012916221
    Node 13    4.171326971458686
    Node 14    2.222293355556507
Sigmoid Node 25
    Inputs    Weights
    Threshold    -6.3256325746421265
    Node 2    -4.0111540511397905
    Node 3    5.40375057371916
    Node 4    -1.7637287232372831
    Node 5    2.1206330784138077
    Node 6    -0.9475380352381562
    Node 7    1.4056357812674805
    Node 8    2.3167731597381285
    Node 9    -3.0714491512402553
    Node 10    1.427637497332509
    Node 11    1.1150734005447285
    Node 12    4.630786841037741
    Node 13    5.995952717650056
    Node 14    1.005794890295149
Sigmoid Node 26
    Inputs    Weights
    Threshold    -5.42538193069266
    Node 2    5.822390102573891
    Node 3    2.5819705997314317
    Node 4    -6.464915485951374
    Node 5    -1.7094317818131626
    Node 6    -0.14422221659591117
    Node 7    -1.2251001651123663
    Node 8    -2.626994814166
    Node 9    -1.688843566431575
    Node 10    -2.9459153998631207
    Node 11    4.791259634626282
    Node 12    0.026372817905665594
    Node 13    2.96287115239236
    Node 14    0.6706307282166308
Sigmoid Node 27
    Inputs    Weights
    Threshold    -4.001536672550387
    Node 2    6.708604785305681
    Node 3    1.464606580498902
    Node 4    -4.781514475015537
    Node 5    0.9925918736068425
    Node 6    -2.079067390664503
    Node 7    1.4292009634465768
    Node 8    -5.709435732177752
    Node 9    -0.06315429868383793
    Node 10    -4.850536768633128
    Node 11    4.692842268878288
    Node 12    1.1582680521233915
    Node 13    1.8841037432468786
    Node 14    4.785455683997158
Sigmoid Node 28
    Inputs    Weights
    Threshold    1.1714847022944914
    Node 15    -1.656039648023117
    Node 16    -0.7458342938593499
    Node 17    -2.128984951460989
    Node 18    -0.7900629301669289
    Node 19    -1.8169270298811693
    Node 20    -0.19150325405642857
    Node 21    -0.48860766870844413
    Node 22    -0.8042269390192817
    Node 23    0.060556727533625454
    Node 24    -1.776436793785643
    Node 25    -0.133225955689424
    Node 26    0.8679998545563427
    Node 27    -0.1007924663279227
Sigmoid Node 29
    Inputs    Weights
    Threshold    1.7569958327539323
    Node 15    -1.7702153508177167
    Node 16    -1.0930092490145988
    Node 17    -2.6158529911547985
    Node 18    -0.8064088179774918
    Node 19    -1.6909365185883274
    Node 20    -0.2086636844368047
    Node 21    -0.39989074384443735
    Node 22    -0.6318460182994543
    Node 23    -0.049249925862683064
    Node 24    -1.4381353921847382
    Node 25    -0.30873781238631953
    Node 26    0.7543675106299312
    Node 27    -0.5614261367529633
Sigmoid Node 30
    Inputs    Weights
    Threshold    2.132393072405128
    Node 15    -1.4832590208369811
    Node 16    -1.1833123834191557
    Node 17    -2.9494125275503236
    Node 18    -1.0209574021201124
    Node 19    -1.6309169413301337
    Node 20    -0.2720854720947903
    Node 21    -0.4282476933172947
    Node 22    -0.6531565096440619
    Node 23    -0.1963852886974804
    Node 24    -1.0863260942272333
    Node 25    -0.7038832195937693
    Node 26    0.5918459386793199
    Node 27    -0.800370912945568
Sigmoid Node 31
    Inputs    Weights
    Threshold    3.2532111212554957
    Node 15    -1.6528057390344748
    Node 16    -1.928648939150527
    Node 17    -3.9834899017043774
    Node 18    -1.88611008223432
    Node 19    -2.1672676451258046
    Node 20    -0.3267232364104557
    Node 21    -0.6041556050808028
    Node 22    0.49351292812321623
    Node 23    -0.3651663143257029
    Node 24    -0.7884403950317058
    Node 25    -1.7113120553820589
    Node 26    0.32246035035390325
    Node 27    -2.0112302482960627
Sigmoid Node 32
    Inputs    Weights
    Threshold    1.8878671397649143
    Node 15    -1.644420646847494
    Node 16    -1.1585766679377718
    Node 17    -2.626030542726827
    Node 18    -0.8620452834511593
    Node 19    -1.7065484725352862
    Node 20    -0.29516060021601886
    Node 21    -0.5282851666779887
    Node 22    -0.6358514703482749
    Node 23    -0.12947470043641357
    Node 24    -1.266700477357807
    Node 25    -0.4729763317654503
    Node 26    0.7065451903102459
    Node 27    -0.6646143738662532
Sigmoid Node 33
    Inputs    Weights
    Threshold    1.1724699551820672
    Node 15    -1.8605527060734517
    Node 16    -0.6824621274603385
    Node 17    -2.1794405162670003
    Node 18    -0.621545704146997
    Node 19    -1.7508917927493932
    Node 20    -0.1820113102276792
    Node 21    -0.40703842863253115
    Node 22    -0.6668067375373098
    Node 23    0.15239274701191421
    Node 24    -1.933079253074198
    Node 25    -0.06279770011044107
    Node 26    0.9376065972956467
    Node 27    -0.1251854207061975
Sigmoid Node 34
    Inputs    Weights
    Threshold    3.304668902506229
    Node 15    -1.529250664316854
    Node 16    -1.8747062585838643
    Node 17    -4.122194531093945
    Node 18    -2.0300259800982903
    Node 19    -2.218876797651292
    Node 20    -0.3460604197727344
    Node 21    -0.5892403141237916
    Node 22    0.572395428229309
    Node 23    -0.4404217643105661
    Node 24    -0.8280567740339261
    Node 25    -1.809794830431116
    Node 26    0.31157166506081085
    Node 27    -2.027705960695569
Sigmoid Node 35
    Inputs    Weights
    Threshold    8.65811944047069
    Node 15    -2.567021969233527
    Node 16    -0.43854475478012245
    Node 17    -2.9445816906724396
    Node 18    -4.617686522028272
    Node 19    -2.175972812975885
    Node 20    -2.8556228486524544
    Node 21    -0.010151472660446545
    Node 22    -2.230426581708892
    Node 23    -3.675732389836238
    Node 24    -0.6873053762290996
    Node 25    -4.169512854638514
    Node 26    3.3827100853380556
    Node 27    2.1926895433603573
Sigmoid Node 36
    Inputs    Weights
    Threshold    1.2931346557831278
    Node 15    -1.7536984069115154
    Node 16    -0.7409004325567169
    Node 17    -2.207471653641701
    Node 18    -0.7016239082266728
    Node 19    -1.7356986253784137
    Node 20    -0.17320144832103782
    Node 21    -0.5353585685474997
    Node 22    -0.8218653859570325
    Node 23    0.019655482169870713
    Node 24    -1.7141488229223572
    Node 25    -0.16524843884639567
    Node 26    0.8442112959833665
    Node 27    -0.1431303020686085
Sigmoid Node 37
    Inputs    Weights
    Threshold    1.5892126459790619
    Node 15    -1.7404584029878678
    Node 16    -0.984518200970551
    Node 17    -2.443569473918764
    Node 18    -0.7399854403863529
    Node 19    -1.7496779183966151
    Node 20    -0.20841422543698587
    Node 21    -0.4536086804447422
    Node 22    -0.6753958881673673
    Node 23    -7.956578631606259E-4
    Node 24    -1.5463546612629724
    Node 25    -0.24917305862212302
    Node 26    0.8259912011454081
    Node 27    -0.42191512449374713
Sigmoid Node 38
    Inputs    Weights
    Threshold    1.1371764594411218
    Node 15    -1.7794632727709865
    Node 16    -0.7376107603715251
    Node 17    -2.095728609849876
    Node 18    -0.6552581168477284
    Node 19    -1.8043796012818034
    Node 20    -0.22761922277451188
    Node 21    -0.5015336936601222
    Node 22    -0.580973685559692
    Node 23    0.09786679859782632
    Node 24    -1.9588569552940855
    Node 25    -0.024212112392840938
    Node 26    0.9826405946444091
    Node 27    -0.13591050467225518
Sigmoid Node 39
    Inputs    Weights
    Threshold    1.5770888824464822
    Node 15    -1.7908835213522076
    Node 16    -0.9389165059603394
    Node 17    -2.3931003165641918
    Node 18    -0.742561053193254
    Node 19    -1.639746968155199
    Node 20    -0.21835426998748358
    Node 21    -0.48152229271489566
    Node 22    -0.6711822339888912
    Node 23    -0.05504769847193385
    Node 24    -1.576586832379005
    Node 25    -0.24118947377924138
    Node 26    0.7742560201961358
    Node 27    -0.4538126930186783
Sigmoid Node 40
    Inputs    Weights
    Threshold    1.1274715896424046
    Node 15    -1.7563047159243057
    Node 16    -0.7468147704744079
    Node 17    -2.0972171323292343
    Node 18    -0.7181766646178258
    Node 19    -1.7083998446075814
    Node 20    -0.2173970755402698
    Node 21    -0.4461773673912203
    Node 22    -0.7418788578761678
    Node 23    0.07423158205844643
    Node 24    -1.88458367610292
    Node 25    -0.11446125061417979
    Node 26    0.8888702048977467
    Node 27    -0.11320739689308053
Class defective
    Input
    Node 0
Class clean
    Input
    Node 1


Time taken to build model: 272.98 seconds

=== Evaluation on test set ===

Time taken to test model on supplied test set: 0.05 seconds

=== Summary ===

Correctly Classified Instances        3857               85.2187 %
Incorrectly Classified Instances       669               14.7813 %
Kappa statistic                          0.372 
Mean absolute error                      0.209 
Root mean squared error                  0.3464
Relative absolute error                 66.0271 %
Root relative squared error             89.6685 %
Total Number of Instances             4526     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,327    0,032    0,694      0,327    0,445      0,407    0,792     0,527     defective
                 0,968    0,673    0,867      0,968    0,915      0,407    0,792     0,934     clean
Weighted Avg.    0,852    0,557    0,836      0,852    0,830      0,407    0,792     0,860     

=== Confusion Matrix ===

    a    b   <-- classified as
  268  551 |    a = defective
  118 3589 |    b = clean

