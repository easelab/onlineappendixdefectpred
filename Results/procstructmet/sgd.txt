=== Run information ===

Scheme:       weka.classifiers.functions.SGD -F 0 -L 0.01 -R 1.0E-4 -E 500 -C 0.001 -S 1
Relation:     dataset_procstructmet_train
Instances:    10209
Attributes:   15
              fcomm
              fadev
              fddev
              fexp
              foexp
              fmodd
              fnloc
              fcyco
              faddl
              freml
              scat
              tanga
              ndep
              lofc
              label
Test mode:    user supplied test set:  size unknown (reading incrementally)

=== Classifier model (full training set) ===

Loss function: Hinge loss (SVM)

label = 

       -12.0163 (normalized) fcomm
 +     -21.279  (normalized) fadev
 +      -0.7539 (normalized) fddev
 +      -7.5292 (normalized) fexp
 +     -16.425  (normalized) foexp
 +      -8.8883 (normalized) fmodd
 +      -0.7261 (normalized) fnloc
 +       0.3596 (normalized) fcyco
 +       7.0688 (normalized) faddl
 +      13.4534 (normalized) freml
 +       0.2251 (normalized) scat
 +       0.5698 (normalized) tanga
 +      -0.6989 (normalized) ndep
 +       0.2613 (normalized) lofc
 +       1.01  

Time taken to build model: 0.4 seconds

=== Evaluation on test set ===

Time taken to test model on supplied test set: 0.01 seconds

=== Summary ===

Correctly Classified Instances        3027               66.8802 %
Incorrectly Classified Instances      1499               33.1198 %
Kappa statistic                          0.0533
Mean absolute error                      0.3312
Root mean squared error                  0.5755
Relative absolute error                104.6243 %
Root relative squared error            148.9877 %
Total Number of Instances             4526     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,316    0,253    0,216      0,316    0,257      0,055    0,531     0,192     defective
                 0,747    0,684    0,832      0,747    0,787      0,055    0,531     0,829     clean
Weighted Avg.    0,669    0,606    0,720      0,669    0,691      0,055    0,531     0,713     

=== Confusion Matrix ===

    a    b   <-- classified as
  259  560 |    a = defective
  939 2768 |    b = clean

