# Script to perform metrics calculation for all 12 software projects.
# Results will be saved in MySQL tables.
# Usage of external library MySQL Connector.
# Further information about the script can be taken from the readme in the online appendix.
# By Stefan Strueder, 2020.

import csv
import mysql.connector

#########################
##### CONFIGURATION #####
#########################

# PLEASE NOTE BEFORE EXECUTING THIS SCRIPT, THAT ALL 1-processing_... SCRIPTS HAVE BEEN EXECUTED SUCCESSFULL.

# MySQL-database information
host = "ENTER_HERE"
user = "ENTER_HERE"
password = "ENTER_HERE"
database = "ENTER_HERE"
softwares = ["blender", "busybox", "emacs", "gimp", "gnumeric", "gnuplot", "irssi", "libxml2", "lighttpd", "mpsolve", "parrot", "vim"]

#########################
#########################
#########################


#########################
######### MAIN ##########
#########################

print("This script will calculate metrics for your selected software projects.")
print("The processing takes some time depending on the amount of data.")

# Connection to database
target_db = mysql.connector.connect(host = host, user = user, passwd = password)
mycursor = target_db.cursor()

for software in softwares:
	print("Calculating metrics for: " + software)
	sql1 = "CREATE TABLE " + database + "." + software + "_metrics (release_number varchar(255), feature varchar(255), fcomm varchar(255), fadev varchar(255), fddev varchar(255), fexp varchar(255), foexp varchar(255), fmodd varchar(255), fnloc varchar(255), fcyco varchar(255), faddl varchar(255), freml varchar(255))"
	sql2 = "CREATE TABLE " + database + "." + software + "_metrics_new (release_number varchar(255), feature varchar(255), fcomm varchar(255), fadev varchar(255), fddev varchar(255), fexp varchar(255), foexp varchar(255), fmodd varchar(255), fnloc varchar(255), fcyco varchar(255), faddl varchar(255), freml varchar(255))"
	sql3 = "INSERT INTO " + database + "." + software + "_metrics (release_number, feature, fcomm, fadev, fddev, fexp, foexp, fmodd, fnloc, fcyco, faddl, freml) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
	sql4 = "INSERT INTO " + database + "." + software + "_metrics_new (release_number, feature, fcomm, fadev, fddev, fexp, foexp, fmodd, fnloc, fcyco, faddl, freml) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

	# FCOMM, FADEV
	mycursor.execute(sql1)
	sql5 = "SELECT release_number, feature, count(distinct commit_hash) AS fcomm, count(distinct commit_author) AS fadev FROM " + database + "." + software + " WHERE feature != \"none\" GROUP BY release_number, feature"

	mycursor.execute(sql5)
	result_set1 = mycursor.fetchall()

	for row in result_set1:
		val = (row[0], row[1], row[2], row[3], "tbd", "tbd", "tbd", "tbd", "tbd", "tbd", "tbd", "tbd")
		mycursor.execute(sql3, val)
		target_db.commit()

	print("     ... Finished calculating FCOMM and FADEV metrics.")

	# FDDEV
	mycursor.execute(sql2)
	sql6 = "SELECT DISTINCT feature FROM " + database + "." + software + "_metrics"
	mycursor.execute(sql6)
	result_set2 = mycursor.fetchall()

	for row in result_set2:
		feature = row[0]
		sql7 = "SELECT * FROM " + database + "." + software + "_metrics WHERE feature = \"" + feature + "\""
		mycursor.execute(sql7)
		result_set3 = mycursor.fetchall()
		
		fddev = 0
		
		for elem in result_set3:
			fddev = fddev + int(elem[3])
			val = (elem[0], elem[1], elem[2], elem[3], fddev, "tbd", "tbd", "tbd", "tbd", "tbd", "tbd", "tbd")
			mycursor.execute(sql4, val)
			target_db.commit()
			
	print("     ... Finished calculating FDDEV metric.")

	# FEXP
	sql8 = "DROP TABLE " + database + "." + software + "_metrics"
	sql9 = "RENAME TABLE " + database + "." + software + "_metrics_new to " + database + "." + software + "_metrics"
	mycursor.execute(sql8)
	mycursor.execute(sql9)
	mycursor.execute(sql2)

	sql10 = "SELECT DISTINCT release_number FROM " + database + "." + software
	mycursor.execute(sql10)
	result_set4 = mycursor.fetchall()
	releaseList = []

	for row in result_set4:
		releaseList.append(row[0])

	sql11 = "SELECT release_number, feature, commit_author, lines_added, lines_removed FROM " + database + "." + software + " WHERE feature = %s AND release_number = %s GROUP BY release_number, feature, commit_author"

	valueList = []
	meanList = []

	for release in releaseList:
		
		sql12 = "SELECT DISTINCT feature FROM " + database + "." + software + " WHERE feature != \"none\" AND release_number = \"" + release + "\""
		
		mycursor.execute(sql12)
		result_set5 = mycursor.fetchall()
		
		featureList = []
		
		for elem in result_set5:
			featureList.append(elem[0])

		for feature in featureList:
			
			val = (feature, release)
			mycursor.execute(sql11, val)
			result_set6 = mycursor.fetchall()

			if len(result_set6) == 0:
				continue
			else:
				val = 1
				counter = 0
				for elem in result_set6:
					add = int(elem[3])
					rem = int(elem[4])
					if add == 0:
						add = 1
					if rem == 0:
						rem = 1
					prod = add * rem
					val = val * prod
					counter = counter + 1
				mean = val**(1/float(counter))
				value = release + ":::" + feature
				valueList.append(value)
				meanList.append(mean)

	sql13 = "SELECT * FROM " + database + "." + software + "_metrics"

	mycursor.execute(sql13)
	result_set7 = mycursor.fetchall()

	for e in result_set7:
		v = e[0] + ":::" + e[1]

		index = valueList.index(v)
		mean = str(int(meanList[index]))
		val = (e[0], e[1], e[2], e[3], e[4], mean, "tbd", "tbd", "tbd", "tbd", "tbd", "tbd")
		mycursor.execute(sql4, val)
		target_db.commit()
		
	print("     ... Finished calculating FEXP metric.")

	# FOEXP
	mycursor.execute(sql8)
	mycursor.execute(sql9)
	mycursor.execute(sql2)

	sql14 = "SELECT release_number, feature, MAX(commit_author) FROM " + database + "." + software + " WHERE feature != \"none\" GROUP BY release_number, feature"
		
	mycursor.execute(sql14)
	result_set8 = mycursor.fetchall()

	valueList = []
	foexpList = []

	for row in result_set8:
		release = row[0]
		feature = row[1]
		author = row[2]
		
		sql15 = "SELECT lines_added, lines_removed FROM " + database + "." + software + " WHERE release_number = \"" + release + "\" AND feature = \"" + feature + "\" and commit_author = \"" + author + "\""
		
		mycursor.execute(sql15)
		result_set9 = mycursor.fetchall()
		
		foexp = 0
		
		for elem in result_set9:
			fexp = int(elem[0]) + int(elem[1])
			foexp = fexp + foexp
		
		valueList.append(release + ":::" + feature)
		foexpList.append(str(foexp))
		
	sql16 = "SELECT * FROM " + database + "." + software + "_metrics"

	mycursor.execute(sql16)
	result_set10 = mycursor.fetchall()

	for e in result_set10:
		v = e[0] + ":::" + e[1]

		index = valueList.index(v)
		value = str(int(foexpList[index]))
		
		val = (e[0], e[1], e[2], e[3], e[4], e[5], value, "tbd", "tbd", "tbd", "tbd", "tbd")
		mycursor.execute(sql4, val)
		target_db.commit()
		
	print("     ... Finished calculating FOEXP metric.")

	# FMODD
	mycursor.execute(sql8)
	mycursor.execute(sql9)
	mycursor.execute(sql2)

	sql17 = "select release_number, feature, count(commit_hash) as fmodd from " + database + "." + software + " where feature != \"none\" group by release_number, feature"

	mycursor.execute(sql17)
	result_set11 = mycursor.fetchall()

	valueList = []
	fmoddList = []

	for row in result_set11:
		release = row[0]
		feature = row[1]
		scat = row[2]
		
		value = release + ":::" + feature
		valueList.append(value)
		fmoddList.append(scat)

	mycursor.execute(sql16)
	result_set12 = mycursor.fetchall()

	counter = 0

	for elem in result_set12:
		release = elem[0]
		feature = elem[1]	
		value = release + ":::" + feature

		index = valueList.index(value)
		scat = str(int(fmoddList[index]))
		val = (elem[0], elem[1], elem[2], elem[3], elem[4], elem[5], elem[6], scat, "tbd", "tbd", "tbd", "tbd")
		mycursor.execute(sql4, val)
		target_db.commit()
		counter = counter + 1
			
	print("     ... Finished calculating FMODD metric.")

	# FNLOC, FCYCO, FADDL, FREML
	mycursor.execute(sql8)
	mycursor.execute(sql9)
	mycursor.execute(sql2)

	sql18 = "SELECT release_number, feature FROM " + database + "." + software + "_metrics"
		
	mycursor.execute(sql18)
	result_set13 = mycursor.fetchall()

	valueList = []
	nlocList = []
	cyList = []
	addedList = []
	removedList = []

	for row in result_set13:
		release = row[0]
		feature = row[1]

		sql19 = "SELECT nloc, cycomplexity, lines_added, lines_removed FROM " + database + "." + software + " WHERE release_number = \"" + release + "\" AND feature = \"" + feature + "\""		
		mycursor.execute(sql19)
		result_set14 = mycursor.fetchall()
		
		counter = 0
		nloc = 0
		cycomplexity = 0
		added = 0
		removed = 0
		
		for elem in result_set14:
			
			if elem[0] == "None":
				value1 = 0
			else:
				value1 = int(elem[0])
			
			if elem[1] == "None":
				value2 = 0
			else:
				value2 = int(elem[1])

			value3 = int(elem[2])
			value4 = int(elem[3])
			
			nloc = nloc + value1
			cycomplexity = cycomplexity + value2
			added = added + value3
			removed = removed + value4
			counter = counter + 1
			
		nloc = int(nloc / counter)
		cycomplexity = int(cycomplexity / counter)
		added = int(added / counter)
		removed = int(removed / counter)
		
		value = release + ":::" + feature
		
		valueList.append(value)
		nlocList.append(nloc)
		cyList.append(cycomplexity)
		addedList.append(added)
		removedList.append(removed)

	mycursor.execute(sql16)
	result_set15 = mycursor.fetchall()

	for elem in result_set15:	
		release = elem[0]
		feature = elem[1]	
		val = release + ":::" + feature

		try:
			index = valueList.index(val)
			nloc = str(int(nlocList[index]))
			cycomplexity = str(int(cyList[index]))
			added = str(int(addedList[index]))
			removed = str(int(removedList[index]))
			val = (elem[0], elem[1], elem[2], elem[3], elem[4], elem[5], elem[6], elem[7], nloc, cycomplexity, added, removed)
			mycursor.execute(sql4, val)
			target_db.commit()
			
		except:
			continue
		
	print("     ... Finished calculating FNLOC, FCYCO, FADDL and FREML metrics.")

	mycursor.execute(sql8)
	mycursor.execute(sql9)
	
	print("Metrics calculation finished for: " + software)
	
	# Output csv file for Featracer
	sql20 = "SELECT DISTINCT release_number FROM " + database + "." + software
	mycursor.execute(sql20)
	result_set16 = mycursor.fetchall()
		
	hash_list = []
	first_list = []
	release_list = []
		
	for release in result_set16:
		sql21 = "SELECT DISTINCT commit_hash FROM " + database + "." + software + " WHERE release_number = \"" + release[0] + "\""
		mycursor.execute(sql21)
		result_set17 = mycursor.fetchall()
		hash_list.append(result_set17[-1])
		first_list.append(result_set17[0])
		release_list.append(release)
		
	filename = "structure_metrics_files/" + software + ".csv"
	counter = 0
		
	with open(filename, mode = "w", newline = "") as csvoutput:
		writer = csv.writer(csvoutput, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_MINIMAL)
		for elem in hash_list:
			rel = release_list[counter]
			first = first_list[counter]
			writer.writerow([rel[0], first[0], elem[0]])
			counter = counter + 1

	print("Created Featracer .csv file for: " + software)

print("Metrics calculation completed.")

#########################
#########################
#########################