# Script to retrieve and process commit data from Emacs.
# Results will be saved in MySQL tables.
# Usage of external libraries MySQL Connector and PyDriller.
# Further information about the script can be taken from the readme in the online appendix.
# By Stefan Strueder, 2020.

from pydriller import RepositoryMining
from pydriller.git_repository import GitRepository
import mysql.connector
import ntpath
import re

#########################
##### CONFIGURATION #####
#########################

# PLEASE NOTE BEFORE EXECUTING THE SCRIPT
# Clone the following repository into the same folder as this script:
# https://github.com/emacs-mirror/emacs

# MySQL-database information
host = "ENTER_HERE"
user = "ENTER_HERE"
password = "ENTER_HERE"
database = "ENTER_HERE"

# Data retrieval
software = "emacs"
path = "emacs"
releases = ["25.0", "25.1", "25.2", "25.3", "26.0", "26.1", "26.2"]
from_tags = ["emacs-25.0.90", "emacs-25.1", "emacs-25.2", "emacs-25.3", "emacs-26.0.90", "emacs-26.1", "emacs-26.2"]
to_tags = ["emacs-25.1-rc1", "emacs-25.2-rc1", "emacs-25.3", "emacs-26.0.90", "emacs-26.1-rc1", "emacs-26.2", "emacs-26.3-rc1"]

# Data processing
keep_szz_tables = False

#########################
#########################
#########################


#########################
#### HELPER FUNCTION ####
#########################

def findWord(word):
    return re.compile(r'\b({0})\b'.format(word), flags=re.IGNORECASE).search

#########################
#########################
#########################


#########################
######### MAIN ##########
#########################

print("This script extracts and processes commit data for " + software + ".")
print("The processing takes some time depending on the amount of data for the selected releases.")
print("First step: Data retrieval.")

# Creation of database
target_db = mysql.connector.connect(host = host, user = user, passwd = password)
mycursor = target_db.cursor()

try:
	mycursor.execute("CREATE DATABASE " + database)
except:
	print("DB already exists, skipped.")
	
mycursor.execute("CREATE TABLE " + database + "." + software + " (release_number varchar(255), commit_hash longtext, commit_author varchar(255), filename varchar(255), nloc varchar(255), cycomplexity varchar(255), lines_added varchar(255), lines_removed varchar(255), corrective varchar(255), feature longtext)")

# Data retrieval with PyDriller followed by processing steps
sql1 = "INSERT INTO " + database + "." + software + " (release_number, commit_hash, commit_author, filename, nloc, cycomplexity, lines_added, lines_removed, corrective, feature) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

counter = 0
	
try:
	for release in releases:
		commit_no = 1
		for c in RepositoryMining(path, from_tag = from_tags[counter], to_tag = to_tags[counter], only_no_merge = True, only_in_branch = "master").traverse_commits():
			for m in c.modifications:
			
				msg = str(c.msg).lower()
				first_line = msg.partition("\n")[0]
					
				if findWord("fix")(first_line) or findWord("bug")(first_line) or findWord("error")(first_line) or findWord("fail")(first_line) or findWord("fixes")(first_line) or findWord("fixed")(first_line) or findWord("bugfix")(first_line) or findWord("bugs")(first_line):
					corrective = "true"
				else:
					corrective = "false"
			
				diff = str(m.diff)
				diff = diff.lower()
				regex1 = "\s*((#\s*ifdef\s+.*)|(#\s*ifndef\s+.*))"
				regex2 = "(#\s*ifdef\s)|(#\s*ifndef\s)"
				match = re.findall(regex1, diff)
				
				if match:
					match = list(dict.fromkeys(match))
					for entry in match:
						entry = list(dict.fromkeys(entry))
						for e in entry:
							if e == "":
								continue
							else:
								test = re.split(regex2, e)
								if test[1] != None and test[2] == None:
									feature = test[3]			
									val = (release, c.hash, c.author.name, m.filename, str(m.nloc), str(m.complexity), str(m.added), str(m.removed), corrective, feature)
									mycursor.execute(sql1, val)
									target_db.commit()
								elif test[2] != None and test[1] == None:
									feature = "not " + test[3]		
									val = (release, c.hash, c.author.name, m.filename, str(m.nloc), str(m.complexity), str(m.added), str(m.removed), corrective, feature)
									mycursor.execute(sql1, val)
									target_db.commit()
				else:
					feature = "none"
					val = (release, c.hash, c.author.name, m.filename, str(m.nloc), str(m.complexity), str(m.added), str(m.removed), corrective, "none")
					mycursor.execute(sql1, val)
					target_db.commit()
				
			print("     ... Commit number " + str(commit_no) + " for release " + release + " inserted.")
			commit_no = commit_no + 1
		counter = counter + 1
except Exception as e:
	print(e)

sql2 = "DELETE FROM " + database + "." + software + " where feature like \"%\_h\" or feature like \"%\_h\_%\""
mycursor.execute(sql2)
target_db.commit()

# Application of the SZZ-algorithm
print("Second step: Data processing.")

mycursor.execute("CREATE TABLE " + database + "." + software + "_szz (commit_hash longtext, filename longtext, bug_introducing longtext)")

sql3 = "SELECT distinct commit_hash FROM " + database + "." + software + " where corrective = \"true\" and feature != \"none\""
sql4 = "INSERT INTO " + database + "." + software + "_szz (commit_hash, filename, bug_introducing) VALUES (%s, %s, %s)"

mycursor.execute(sql3)
result_set1 = mycursor.fetchall()
gr = GitRepository(path)
counter = 0

for row in result_set1:
	try:
		buggy_commits = gr.get_commits_last_modified_lines(gr.get_commit(row[0]))
		if len(buggy_commits) == 0:
			continue
		else:
			items = buggy_commits.items()
			for item in items:
				filename = item[0]
				filename = ntpath.basename(filename)
				val = (row[0], filename, str(item[1]))
				mycursor.execute(sql4, val)
				target_db.commit()
				counter = counter + 1
				print("     ... " + str(counter) + " successful hit(s) for SZZ-algorithm.")
	except Exception as e:
		print(e)
		continue
		
# Transfer of the results to the main table
mycursor.execute("CREATE TABLE " + database + "." + software + "_new (release_number varchar(255), commit_hash longtext, commit_author varchar(255), filename varchar(255), nloc varchar(255), cycomplexity varchar(255), lines_added varchar(255), lines_removed varchar(255), corrective varchar(255), bug_introducing varchar(255), feature longtext)")

sql5 = "SELECT * FROM " + database + "." + software + "_szz"

mycursor.execute(sql5)
result_set2 = mycursor.fetchall()
szzlist = []
counter = 0

for row in result_set2:
	filename = row[1]
	bugs = list(row[2].replace("{", "").replace("}", "").replace("'", "").replace(" ", "").split(","))
	
	for elem in bugs:
		szzlist.append(filename + ":::" + elem)
		
sql6 = "SELECT * FROM " + database + "." + software
mycursor.execute(sql6)
result_set3 = mycursor.fetchall()

for row in result_set3:
	name = row[3]
	hash = row[1]
	
	concat = name + ":::" + hash
	sql7 = "INSERT INTO " + database + "." + software + "_new (release_number, commit_hash, commit_author, filename, nloc, cycomplexity, lines_added, lines_removed, corrective, bug_introducing, feature) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
	
	try:
		if concat in szzlist:
			val = (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], "true", row[9])
			mycursor.execute(sql7, val)
			target_db.commit()
			counter = counter + 1
		else:
			val = (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], "false", row[9])
			mycursor.execute(sql7, val)
			target_db.commit()
			counter = counter + 1
	except:
		if concat in szzlist:
			val = (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], "true", "infinity")
			mycursor.execute(sql7, val)
			target_db.commit()
			counter = counter + 1
		else:
			val = (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], "false", "infinity")
			mycursor.execute(sql7, val)
			target_db.commit()
			counter = counter + 1
			
sql8 = "DROP TABLE " + database + "." + software
sql9 = "RENAME TABLE " + database + "." + software + "_new to " + database + "." + software
mycursor.execute(sql8)
mycursor.execute(sql9)

if keep_szz_tables == False:
	sql10 = "DROP TABLE " + database + "." + software + "_szz"
	mycursor.execute(sql10)
	print("SZZ-table has been dropped.")
	
print("Data retrieval and processing finished.")

#########################
#########################
#########################