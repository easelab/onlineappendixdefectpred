# Script to perform finalization of dataset creation.
# Final datasets will be exported as .arff files for WEKA.
# Usage of external library MySQL Connector.
# Further information about the script can be taken from the readme in the online appendix.
# By Stefan Strueder, 2020.

import csv
import mysql.connector

#########################
##### CONFIGURATION #####
#########################

# MySQL-database information
host = "ENTER_HERE"
user = "ENTER_HERE"
password = "ENTER_HERE"
database = "ENTER_HERE"
softwares = ["ENTER_HERE"]

# Split information
train_releases = [["ENTER_HERE"]]
test_releases = [["ENTER_HERE"]]

# Further configurations
keep_main = False
keep_metrics = False
keep_final = False

#########################
#########################
#########################


#########################
### HELPER  FUNCTIONS ###
#########################
	
def Average(lst): 
    return round(sum(lst) / len(lst), 0)

#########################
#########################
#########################


#########################
######### MAIN ##########
#########################

print("This script will finalize the creation of the three datasets based on the software you have chosen.")
print("This process takes some time depending on the amount of data.")

# Connection to database
target_db = mysql.connector.connect(host = host, user = user, passwd = password)
mycursor = target_db.cursor()

software_counter = 0

sql19 = "CREATE TABLE " + database + ".final_train (fcomm varchar(255), fadev varchar(255), fddev varchar(255), fexp varchar(255), foexp varchar(255), fmodd varchar(255), fnloc varchar(255), fcyco varchar(255), faddl varchar(255), freml varchar(255), scat varchar(255), tanga varchar(255), ndep varchar(255), lofc varchar(255), label varchar(255))"
mycursor.execute(sql19)
	
sql20 = "CREATE TABLE " + database + ".final_test (fcomm varchar(255), fadev varchar(255), fddev varchar(255), fexp varchar(255), foexp varchar(255), fmodd varchar(255), fnloc varchar(255), fcyco varchar(255), faddl varchar(255), freml varchar(255), scat varchar(255), tanga varchar(255), ndep varchar(255), lofc varchar(255), label varchar(255))"
mycursor.execute(sql20)
	

# Finalization of dataset
for software in softwares:
	sql1 = "CREATE TABLE " + database + "." + software + "_final (release_number varchar(255), feature varchar(255), fcomm varchar(255), fadev varchar(255), fddev varchar(255), fexp varchar(255), foexp varchar(255), fmodd varchar(255), fnloc varchar(255), fcyco varchar(255), faddl varchar(255), freml varchar(255), label varchar(255))"
	sql2 = "CREATE TABLE " + database + "." + software + "_final_new (release_number varchar(255), feature varchar(255), fcomm varchar(255), fadev varchar(255), fddev varchar(255), fexp varchar(255), foexp varchar(255), fmodd varchar(255), fnloc varchar(255), fcyco varchar(255), faddl varchar(255), freml varchar(255), label varchar(255))"
	sql3 = "INSERT INTO " + database + "." + software + "_final (release_number, feature, fcomm, fadev, fddev, fexp, foexp, fmodd, fnloc, fcyco, faddl, freml, label) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
	sql4 = "INSERT INTO " + database + "." + software + "_final_new (release_number, feature, fcomm, fadev, fddev, fexp, foexp, fmodd, fnloc, fcyco, faddl, freml, label) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

	mycursor.execute(sql1)
	
	sql5 = "SELECT * FROM " + database + "." + software + "_metrics"
	
	mycursor.execute(sql5)
	result_set1 = mycursor.fetchall()
	
	counter = 0
	
	for row in result_set1:
		release = row[0]
		feature = row[1]
		fcomm = row[2]
		fadev = row[3]
		fddev = row[4]
		fexp = row[5]
		foexp = row[6]
		fmodd = row[7]
		fnloc = row[8]
		fcyco = row[9]
		faddl = row[10]
		freml = row[11]
		
		sql6 = "SELECT corrective, bug_introducing FROM " + database + "." + software + " WHERE release_number = \"" + release + "\" and feature = \"" + feature + "\""
		mycursor.execute(sql6)
		result_set2 = mycursor.fetchall()
		
		for elem in result_set2:
			corrective = elem[0]
			bug_introducing = elem[1]
			
			if corrective == "false" and bug_introducing == "false":
				label = "clean"
			elif corrective == "false" and bug_introducing == "true":
				label = "defective"
			elif corrective == "true" and bug_introducing == "false":
				label = "clean"
			elif corrective == "true" and bug_introducing == "true":
				label = "defective"
				
			val = (release, feature, fcomm, fadev, fddev, fexp, foexp, fmodd, fnloc, fcyco, faddl, freml, label)

			mycursor.execute(sql3, val)
			target_db.commit()
			counter = counter + 1
			
	print("     ... Finished first step of finalizing.")
	
	mycursor.execute(sql2)	
	sql7 = "SELECT release_number, feature FROM " + database + "." + software + "_final group by release_number, feature"
	
	mycursor.execute(sql7)
	result_set3 = mycursor.fetchall()
	
	counter = 0
	
	for row in result_set3:
		release = row[0]
		feature = row[1]
		
		sql8 = "SELECT * FROM " + database + "." + software + "_final where release_number = \"" + release + "\" AND feature = \"" + feature + "\"" 
		mycursor.execute(sql8)
		result_set4 = mycursor.fetchall()
		
		fcomm_mean = 0
		fadev_mean = 0
		fddev_mean = 0
		fexp_mean = 0
		foexp_mean = 0
		fmodd_mean = 0
		fnloc_mean = 0
		fcyco_mean = 0
		faddl_mean = 0
		freml_mean = 0
		label_list = []
		count = 0
		
		for elem in result_set4:
			fcomm = int(elem[2])
			fadev = int(elem[3])
			fddev = int(elem[4])
			fexp = int(elem[5])
			foexp = int(elem[6])
			fmodd = int(elem[7])
			fnloc = int(elem[8])
			fcyco = int(elem[9])
			faddl = int(elem[10])
			freml = int(elem[11])
			
			fcomm_mean = fcomm_mean + fcomm
			fadev_mean = fadev_mean + fadev
			fddev_mean = fddev_mean + fddev
			ffexp_mean = fexp_mean + fexp
			foexp_mean = foexp_mean + foexp
			fmodd_mean = fmodd_mean + fmodd
			fnloc_mean = fnloc_mean + fnloc
			fcyco_mean = fcyco_mean + fcyco
			faddl_mean = faddl_mean + faddl
			freml_mean = freml_mean + freml
			count = count + 1
			label_list.append(elem[12])
			
		fcomm_mean = int(fcomm_mean / count)
		fadev_mean = int(fadev_mean / count)
		fddev_mean = int(fddev_mean / count)
		fexp_mean = int(fexp_mean / count)
		foexp_mean = int(foexp_mean / count)
		fmodd_mean = int(fmodd_mean / count)		
		fnloc_mean = int(fnloc_mean / count)
		fcyco_mean = int(fcyco_mean / count)
		faddl_mean = int(faddl_mean / count)
		freml_mean = int(freml_mean / count)
		
		if "defective" in label_list:
			label = "defective"
		else:
			label = "clean"
			
		val = (release, feature, fcomm_mean, fadev_mean, fddev_mean, fexp_mean, foexp_mean, fmodd_mean, fnloc_mean, fcyco_mean, faddl_mean, freml_mean, label)

		mycursor.execute(sql4, val)
		target_db.commit()
		counter = counter + 1

	print("     ... Finished second step of finalizing.")

	sql9 = "DROP TABLE " + database + "." + software + "_final"
	sql10 = "RENAME TABLE " + database + "." + software + "_final_new to " + database + "." + software + "_final"
	mycursor.execute(sql9)
	mycursor.execute(sql10)
	
# Import of structural metrics
	sql11 = "CREATE TABLE " + database + "." + software + "_structure (release_number varchar(255), feature varchar(255), scat varchar(255), tanga varchar(255), ndep varchar(255), lofc varchar(255), label varchar(255))"
	mycursor.execute(sql11)
	filename = "structure_metrics_files/" + software + "_metrics_summary.csv"

	with open(filename, newline = '') as csvfile:
		file = csv.reader(csvfile, delimiter = ';', quotechar = '|')
		next(file)
		
		key_list = []
		scat_list = []
		tanga_list = []
		ndep_list = []
		lofc_list = []
		
		for row in file:
			key = row[0] + ":::" + row[2].lower()
			key_list.append(key)
			scat_list.append(float(row[3].replace(",", ".")))
			tanga_list.append(float(row[4].replace(",", ".")))
			ndep_list.append(float(row[5].replace(",", ".")))
			lofc_list.append(float(row[6].replace(",", ".")))
			
	sql12 = "SELECT * FROM " + database + "." + software + "_final"
	sql13 = "INSERT INTO " + database + "." + software + "_structure (release_number, feature, scat, tanga, ndep, lofc, label) VALUES (%s, %s, %s, %s, %s, %s, %s)"
	mycursor.execute(sql12)
	result_set5 = mycursor.fetchall()

	count = 0

	for elem in result_set5:
		key = elem[0] + ":::" + elem[1].lower()

		indices = [i for i, x in enumerate(key_list) if x == key]
		
		if len(indices) != 0:
			
			counter = 0
			scat_mean = []
			tanga_mean = []
			ndep_mean = []
			lofc_mean = []
			
			for index in indices:
				scat_mean.append(scat_list[index])
				tanga_mean.append(tanga_list[index])
				ndep_mean.append(ndep_list[index])
				lofc_mean.append(lofc_list[index])
				counter = counter + 1
				
			scat = Average(scat_mean)
			tanga = Average(tanga_mean)
			ndep = Average(ndep_mean)
			lofc = Average(lofc_mean)
			
			if scat.is_integer():
				scat = int(scat)
				
			if tanga.is_integer():
				tanga = int(tanga)
				
			if ndep.is_integer():
				ndep = int(ndep)
				
			if lofc.is_integer():
				lofc = int(lofc)
			
			val = (elem[0], elem[1], scat, tanga, ndep, lofc, elem[12])
			
			count = count + 1
			
			mycursor.execute(sql13, val)
			target_db.commit()
			
		else:
			val = (elem[0], elem[1], "0", "0", "0", "0", elem[12])
			
			count = count + 1
			
			mycursor.execute(sql13, val)
			target_db.commit()
			
	sql14 = "SELECT * FROM " + database + "." + software + "_final"
	mycursor.execute(sql14)
	result_set6 = mycursor.fetchall()
	
	sql15 = "SELECT * FROM " + database + "." + software + "_structure"
	mycursor.execute(sql15)
	result_set7 = mycursor.fetchall()
	
	sql16 = "CREATE TABLE " + database + "." + software + "_final_new (release_number varchar(255), feature varchar(255), fcomm varchar(255), fadev varchar(255), fddev varchar(255), fexp varchar(255), foexp varchar(255), fmodd varchar(255), fnloc varchar(255), fcyco varchar(255), faddl varchar(255), freml varchar(255), scat varchar(255), tanga varchar(255), ndep varchar(255), lofc varchar(255), label varchar(255))"
	mycursor.execute(sql16)
	
	sql17 = "INSERT INTO " + database + "." + software + "_final_new (release_number, feature, fcomm, fadev, fddev, fexp, foexp, fmodd, fnloc, fcyco, faddl, freml, scat, tanga, ndep, lofc, label) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

	counter = 0
	
	for elem in result_set6:
		result = result_set7[counter]
		rel = elem[0]
		feature = elem[1]
		fcomm = elem[2]
		fadev = elem[3]
		fddev = elem[4]
		fexp = elem[5]
		foexp = elem[6]
		fmodd = elem[7]
		fnloc = elem[8]
		fcyco = elem[9]
		faddl = elem[10]
		freml = elem[11]
		scat = result[2]
		tanga = result[3]
		ndep = result[4]
		lofc = result[5]
		label = elem[12]
		
		val = (rel, feature, fcomm, fadev, fddev, fexp, foexp, fmodd, fnloc, fcyco, faddl, freml, scat, tanga, ndep, lofc, label)

			
		mycursor.execute(sql17, val)
		target_db.commit()
		
		counter = counter + 1
		
	mycursor.execute(sql9)
	mycursor.execute(sql10)
	
	sql18 = "DROP TABLE " + database + "." + software + "_structure"
	mycursor.execute(sql18)
	
	print("     ... Finished importing of structural metrics.")

	
# Split into training and test dataset
	train = train_releases[software_counter]
	test = test_releases[software_counter]
	
	for elem in train:
		sql21 = "SELECT * FROM " + database + "." + software + "_final WHERE release_number = \"" + elem + "\""
		mycursor.execute(sql21)
		result_set8 = mycursor.fetchall()
		
		counter = 0
	
		for elem in result_set8:
			sql22 = "INSERT INTO " + database + ".final_train (fcomm, fadev, fddev, fexp, foexp, fmodd, fnloc, fcyco, faddl, freml, scat, tanga, ndep, lofc, label) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
			val = (elem[2], elem[3], elem[4], elem[5], elem[6], elem[7], elem[8], elem[9], elem[10], elem[11], elem[12], elem[13], elem[14], elem[15], elem[16])

			mycursor.execute(sql22, val)
			target_db.commit()
			counter = counter + 1
			
	for elem in test:
		sql23 = "SELECT * FROM " + database + "." + software + "_final WHERE release_number = \"" + elem + "\""
		mycursor.execute(sql23)
		result_set9 = mycursor.fetchall()
		
		counter = 0
	
		for elem in result_set9:
			sql24 = "INSERT INTO " + database + ".final_test (fcomm, fadev, fddev, fexp, foexp, fmodd, fnloc, fcyco, faddl, freml, scat, tanga, ndep, lofc, label) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
			val = (elem[2], elem[3], elem[4], elem[5], elem[6], elem[7], elem[8], elem[9], elem[10], elem[11], elem[12], elem[13], elem[14], elem[15], elem[16])

			mycursor.execute(sql24, val)
			target_db.commit()
			counter = counter + 1
			
	software_counter = software_counter + 1
	
print("     ... Finished splitting of datasets.")
	
# Dataset procstructmet
sql25 = "SELECT * FROM " + database + ".final_train" 
mycursor.execute(sql25)
result_set10 = mycursor.fetchall()

fcomm_list = []
fadev_list = []
fddev_list = []
fexp_list = []
foexp_list = []
fmodd_list = []
fnloc_list = []
fcyco_list = []
faddl_list = []
freml_list = []
scat_list = []
tanga_list = []
ndep_list = []
lofc_list = []
label_list = []

for elem in result_set10:
	fcomm_list.append(int(elem[0]))
	fadev_list.append(int(elem[1]))
	fddev_list.append(int(elem[2]))
	fexp_list.append(int(elem[3]))
	foexp_list.append(int(elem[4]))
	fmodd_list.append(int(elem[5]))
	fnloc_list.append(int(elem[6]))
	fcyco_list.append(int(elem[7]))
	faddl_list.append(int(elem[8]))
	freml_list.append(int(elem[9]))
	scat_list.append(int(elem[10]))
	tanga_list.append(int(elem[11]))
	ndep_list.append(int(elem[12]))
	lofc_list.append(int(elem[13]))
	label_list.append(elem[14])

filename = "dataset_procstructmet_train.arff"
file = open(filename, "w")

file.write("@relation dataset_procstructmet_train")
file.write("\n")
file.write("\n")
file.write("@attribute fcomm numeric")
file.write("\n")
file.write("@attribute fadev numeric")
file.write("\n")
file.write("@attribute fddev numeric")
file.write("\n")
file.write("@attribute fexp numeric")
file.write("\n")
file.write("@attribute foexp numeric")
file.write("\n")
file.write("@attribute fmodd numeric")
file.write("\n")
file.write("@attribute fnloc numeric")
file.write("\n")
file.write("@attribute fcyco numeric")
file.write("\n")
file.write("@attribute faddl numeric")
file.write("\n")
file.write("@attribute freml numeric")
file.write("\n")
file.write("@attribute scat numeric")
file.write("\n")
file.write("@attribute tanga numeric")
file.write("\n")
file.write("@attribute ndep numeric")
file.write("\n")
file.write("@attribute lofc numeric")
file.write("\n")
file.write("@attribute label {defective,clean}")
file.write("\n")
file.write("\n")
file.write("@data")
file.write("\n")

counter = len(label_list)

for i in range(counter):
	line = (str(fcomm_list[i]) + "," + str(fadev_list[i]) + "," + str(fddev_list[i]) + "," + str(fexp_list[i]) + "," + str(foexp_list[i]) + "," + str(fmodd_list[i]) + "," + str(fnloc_list[i]) + "," + str(fcyco_list[i]) + "," + str(faddl_list[i]) + "," + str(freml_list[i]) + "," + str(scat_list[i]) + "," + str(tanga_list[i]) + "," + str(ndep_list[i]) + "," + str(lofc_list[i]) + "," + label_list[i])
	file.write(line)
	file.write("\n")
	
sql26 = "SELECT * FROM " + database + ".final_test" 
mycursor.execute(sql26)
result_set11 = mycursor.fetchall()

fcomm_list = []
fadev_list = []
fddev_list = []
fexp_list = []
foexp_list = []
fmodd_list = []
fnloc_list = []
fcyco_list = []
faddl_list = []
freml_list = []
scat_list = []
tanga_list = []
ndep_list = []
lofc_list = []
label_list = []

for elem in result_set11:
	fcomm_list.append(int(elem[0]))
	fadev_list.append(int(elem[1]))
	fddev_list.append(int(elem[2]))
	fexp_list.append(int(elem[3]))
	foexp_list.append(int(elem[4]))
	fmodd_list.append(int(elem[5]))
	fnloc_list.append(int(elem[6]))
	fcyco_list.append(int(elem[7]))
	faddl_list.append(int(elem[8]))
	freml_list.append(int(elem[9]))
	scat_list.append(int(elem[10]))
	tanga_list.append(int(elem[11]))
	ndep_list.append(int(elem[12]))
	lofc_list.append(int(elem[13]))
	label_list.append(elem[14])

filename = "dataset_procstructmet_test.arff"
file = open(filename, "w")

file.write("@relation dataset_procstructmet_test")
file.write("\n")
file.write("\n")
file.write("@attribute fcomm numeric")
file.write("\n")
file.write("@attribute fadev numeric")
file.write("\n")
file.write("@attribute fddev numeric")
file.write("\n")
file.write("@attribute fexp numeric")
file.write("\n")
file.write("@attribute foexp numeric")
file.write("\n")
file.write("@attribute fmodd numeric")
file.write("\n")
file.write("@attribute fnloc numeric")
file.write("\n")
file.write("@attribute fcyco numeric")
file.write("\n")
file.write("@attribute faddl numeric")
file.write("\n")
file.write("@attribute freml numeric")
file.write("\n")
file.write("@attribute scat numeric")
file.write("\n")
file.write("@attribute tanga numeric")
file.write("\n")
file.write("@attribute ndep numeric")
file.write("\n")
file.write("@attribute lofc numeric")
file.write("\n")
file.write("@attribute label {defective,clean}")
file.write("\n")
file.write("\n")
file.write("@data")
file.write("\n")

counter = len(label_list)

for i in range(counter):
	line = (str(fcomm_list[i]) + "," + str(fadev_list[i]) + "," + str(fddev_list[i]) + "," + str(fexp_list[i]) + "," + str(foexp_list[i]) + "," + str(fmodd_list[i]) + "," + str(fnloc_list[i]) + "," + str(fcyco_list[i]) + "," + str(faddl_list[i]) + "," + str(freml_list[i]) + "," + str(scat_list[i]) + "," + str(tanga_list[i]) + "," + str(ndep_list[i]) + "," + str(lofc_list[i]) + "," + label_list[i])
	file.write(line)
	file.write("\n")
	
print("     ... Exported procstructmet datasets.")
	
# Dataset procmet
sql27 = "SELECT * FROM " + database + ".final_train" 
mycursor.execute(sql27)
result_set12 = mycursor.fetchall()

fcomm_list = []
fadev_list = []
fddev_list = []
fexp_list = []
foexp_list = []
fmodd_list = []
faddl_list = []
freml_list = []
label_list = []

for elem in result_set12:
	fcomm_list.append(int(elem[0]))
	fadev_list.append(int(elem[1]))
	fddev_list.append(int(elem[2]))
	fexp_list.append(int(elem[3]))
	foexp_list.append(int(elem[4]))
	fmodd_list.append(int(elem[5]))
	faddl_list.append(int(elem[8]))
	freml_list.append(int(elem[9]))
	label_list.append(elem[14])

filename = "dataset_procmet_train.arff"
file = open(filename, "w")

file.write("@relation dataset_procmet_train")
file.write("\n")
file.write("\n")
file.write("@attribute fcomm numeric")
file.write("\n")
file.write("@attribute fadev numeric")
file.write("\n")
file.write("@attribute fddev numeric")
file.write("\n")
file.write("@attribute fexp numeric")
file.write("\n")
file.write("@attribute foexp numeric")
file.write("\n")
file.write("@attribute fmodd numeric")
file.write("\n")
file.write("@attribute faddl numeric")
file.write("\n")
file.write("@attribute freml numeric")
file.write("\n")
file.write("@attribute label {defective,clean}")
file.write("\n")
file.write("\n")
file.write("@data")
file.write("\n")

counter = len(label_list)

for i in range(counter):
	line = (str(fcomm_list[i]) + "," + str(fadev_list[i]) + "," + str(fddev_list[i]) + "," + str(fexp_list[i]) + "," + str(foexp_list[i]) + "," + str(fmodd_list[i]) + "," + str(faddl_list[i]) + "," + str(freml_list[i]) + "," + label_list[i])
	file.write(line)
	file.write("\n")
	
sql28 = "SELECT * FROM " + database + ".final_test" 
mycursor.execute(sql28)
result_set13 = mycursor.fetchall()

fcomm_list = []
fadev_list = []
fddev_list = []
fexp_list = []
foexp_list = []
fmodd_list = []
faddl_list = []
freml_list = []
label_list = []

for elem in result_set13:
	fcomm_list.append(int(elem[0]))
	fadev_list.append(int(elem[1]))
	fddev_list.append(int(elem[2]))
	fexp_list.append(int(elem[3]))
	foexp_list.append(int(elem[4]))
	fmodd_list.append(int(elem[5]))
	faddl_list.append(int(elem[8]))
	freml_list.append(int(elem[9]))
	label_list.append(elem[14])

filename = "dataset_procmet_test.arff"
file = open(filename, "w")

file.write("@relation dataset_procmet_train")
file.write("\n")
file.write("\n")
file.write("@attribute fcomm numeric")
file.write("\n")
file.write("@attribute fadev numeric")
file.write("\n")
file.write("@attribute fddev numeric")
file.write("\n")
file.write("@attribute fexp numeric")
file.write("\n")
file.write("@attribute foexp numeric")
file.write("\n")
file.write("@attribute fmodd numeric")
file.write("\n")
file.write("@attribute faddl numeric")
file.write("\n")
file.write("@attribute freml numeric")
file.write("\n")
file.write("@attribute label {defective,clean}")
file.write("\n")
file.write("\n")
file.write("@data")
file.write("\n")

counter = len(label_list)

for i in range(counter):
	line = (str(fcomm_list[i]) + "," + str(fadev_list[i]) + "," + str(fddev_list[i]) + "," + str(fexp_list[i]) + "," + str(foexp_list[i]) + "," + str(fmodd_list[i]) + "," + str(faddl_list[i]) + "," + str(freml_list[i]) + "," + label_list[i])
	file.write(line)
	file.write("\n")
	
print("     ... Exported procmet datasets.")
	
# Dataset queiroz
sql29 = "SELECT * FROM " + database + ".final_train" 
mycursor.execute(sql29)
result_set14 = mycursor.fetchall()

fcomm_list = []
fadev_list = []
fddev_list = []
fexp_list = []
foexp_list = []
label_list = []

for elem in result_set14:
	fcomm_list.append(int(elem[0]))
	fadev_list.append(int(elem[1]))
	fddev_list.append(int(elem[2]))
	fexp_list.append(int(elem[3]))
	foexp_list.append(int(elem[4]))
	label_list.append(elem[14])

filename = "dataset_queirozmet_train.arff"
file = open(filename, "w")

file.write("@relation dataset_queirozmet_train")
file.write("\n")
file.write("\n")
file.write("@attribute fcomm numeric")
file.write("\n")
file.write("@attribute fadev numeric")
file.write("\n")
file.write("@attribute fddev numeric")
file.write("\n")
file.write("@attribute fexp numeric")
file.write("\n")
file.write("@attribute foexp numeric")
file.write("\n")
file.write("@attribute label {defective,clean}")
file.write("\n")
file.write("\n")
file.write("@data")
file.write("\n")

counter = len(label_list)

for i in range(counter):
	line = (str(fcomm_list[i]) + "," + str(fadev_list[i]) + "," + str(fddev_list[i]) + "," + str(fexp_list[i]) + "," + str(foexp_list[i]) + "," + label_list[i])
	file.write(line)
	file.write("\n")
	
sql30 = "SELECT * FROM " + database + ".final_test" 
mycursor.execute(sql30)
result_set15 = mycursor.fetchall()

fcomm_list = []
fadev_list = []
fddev_list = []
fexp_list = []
foexp_list = []
label_list = []

for elem in result_set15:
	fcomm_list.append(int(elem[0]))
	fadev_list.append(int(elem[1]))
	fddev_list.append(int(elem[2]))
	fexp_list.append(int(elem[3]))
	foexp_list.append(int(elem[4]))
	label_list.append(elem[14])

filename = "dataset_queirozmet_test.arff"
file = open(filename, "w")

file.write("@relation dataset_queirozmet_test")
file.write("\n")
file.write("\n")
file.write("@attribute fcomm numeric")
file.write("\n")
file.write("@attribute fadev numeric")
file.write("\n")
file.write("@attribute fddev numeric")
file.write("\n")
file.write("@attribute fexp numeric")
file.write("\n")
file.write("@attribute foexp numeric")
file.write("\n")
file.write("@attribute label {defective,clean}")
file.write("\n")
file.write("\n")
file.write("@data")
file.write("\n")

counter = len(label_list)

for i in range(counter):
	line = (str(fcomm_list[i]) + "," + str(fadev_list[i]) + "," + str(fddev_list[i]) + "," + str(fexp_list[i]) + "," + str(foexp_list[i]) + "," + label_list[i])
	file.write(line)
	file.write("\n")
	
print("     ... Exported queirozmet datasets.")

for software in softwares:
	if keep_main == False:
		sql31 = "DROP TABLE " + database + "." + software
		mycursor.execute(sql31)
		print("Main table of " + software + " has been dropped.")
		
	if keep_metrics == False:
		sql32 = "DROP TABLE " + database + "." + software + "_metrics"
		mycursor.execute(sql32)
		print("Metrics table of " + software + " has been dropped.")
	
	if keep_final == False:
		sql33 = "DROP TABLE " + database + "." + software + "_final"
		mycursor.execute(sql33)
		print("Final table of " + software + " has been dropped.")

print("Finalization finished.")
	
#########################
#########################
#########################